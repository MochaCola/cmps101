import java.lang.Math;

// Vector3D.java
// A class that implements the Vector3D ADT.

// Notes:
// Angles are always in radians, not degrees.

class Vector3D {

  // Fields
  
  private float m_x;	// x field
  private float m_y;	// y field
  private float m_z;	// z field
  private float m_ang1;	// first angle field (in radians)
  private float m_ang2; // second angle field (in radians)
  private float m_mag;	// magnitude field

  // Constructors

  // The default constructor should create a new Vector3D with no magnitude.
  public Vector3D() {
	  m_x = m_y = m_z = 0;	        // set x, y & z to 0
	  m_ang1 = this.findAng1();     // because x & y are 0, findAng1 will return 0
	  m_mag = this.findMag();	// becasue x,y,z are 0, findMag will return 0
	  m_ang2 = this.findAng2();	// because x,y,z are 0, findAng2 will return 0
  }

  // This constructor takes an x, y, and z coordinate for the Vector3D.
  public Vector3D(float x, float y, float z) {
	  m_x = x;			// set m_x field to x argument
	  m_y = y;			// set m_y field to y argument
	  m_z = z;		     	// set m_z field to z argument
	  m_ang1 = this.findAng1();	// call findAng1 to find ang1
	  m_mag = this.findMag();	// call findMag to find magnitude
          m_ang2 = this.findAng2(); 	// call findAng2 to find ang2
  }

  // This constructor creates a Vector3D from another Vector3D.
  public Vector3D(Vector3D other) {
	  m_x = other.getX();		// m_x = other.m_x
	  m_y = other.getY();		// m_y = other.m_y
	  m_z = other.getZ();		// m_z = other.m_z
	  m_ang1 = other.getAngle1();	// m_ang1 = other.m_ang1
	  m_ang2 = other.getAngle2();	// m_ang2 = other.m_ang2
	  m_mag = other.getMagnitude(); // m_mag = other.m_mag
  }

  // This "constructor" takes two angles and a magnitude for the Vector3D.
  // It is not a traditional constructor because only one function can have
  //   the signature Vector3D(float, float, float).
  public static Vector3D polarVector(float angle1, float angle2, float magnitude) {
          Vector3D temp = new Vector3D();
          temp.m_x = magnitude * ((float) Math.sin(angle2)); // x = rsin(angle2)
	  temp.m_x *= ((float) Math.cos(angle1));	     // x = rsin(angle2)cos(angle1)
          temp.m_y = magnitude * ((float) Math.sin(angle2)); // y = rsin(angle2)
	  temp.m_y *= ((float) Math.sin(angle1));	     // y = rsin(angle2)sin(angle1)
	  temp.m_z = magnitude * ((float) Math.cos(angle2)); // z = rcos(angle2)
          temp.m_ang1 = angle1;                              // angle1 will be the same
	  temp.m_ang2 = angle2;				     // angle2 will be the same
          temp.m_mag = magnitude;                            // magnitude will be the same
          return temp;                                       // return temp Vector3D
  }



  // Access functions

  /** getX
   *  Returns the x coordinate of the Vector3D.
   */
  public float getX() {
	  return m_x; // return m_x field
  }

  /** getY
   *  Returns the y coordinate of the Vector3D.
   */
  public float getY() {
	  return m_y; // return m_y field
  }

  /** getZ
   *  Returns the z coordinate of the Vector3D.
   */
  public float getZ() {
          return m_z; // return m_z field
  }

  /** getAngle1
   *  Returns the first angle of the Vector3D.
   */
  public float getAngle1() {
	  return m_ang1; // return m_ang1 field
  }

  /** getAngle2
   *  Returns the second angle of the Vector3D.
   */
  public float getAngle2() {
          return m_ang2; // return m_ang2 field
  }

  /** getMagnitude
   *  Returns the magnitude of the Vector3D.
   */
  public float getMagnitude() {
	  return m_mag; // return m_mag field
  }

  /** add
   *  Returns the sum of this Vector3D with the given Vector3D.
   */
  public Vector3D add(Vector3D other) {
	  Vector3D sum = new Vector3D(this);	// set new Vector3D = operand so operand won't change
	  sum.m_x += other.getX();		// add x to other x
	  sum.m_y += other.getY();		// add y to other y
	  sum.m_z += other.getZ();		// add z to other z
	  sum.m_ang1 = sum.findAng1();		// call findAng1 to find new first angle
	  sum.m_mag = sum.findMag();		// call findMag to find new magnitude
	  sum.m_ang2 = sum.findAng2();		// call findAng2 to find new second angle
	  return sum;				// return sum Vector3D
  }

  /** subtract
   *  Returns the difference between this Vector3D and the given Vector3D.
   */
  public Vector3D subtract(Vector3D other) {
	  Vector3D dif = new Vector3D(this);	// set new Vector3D = operand so operand won't change
	  dif.m_x -= other.getX();		// subtract x by other x
	  dif.m_y -= other.getY();		// subtract y by other x
	  dif.m_z -= other.getZ();		// subtract z by other z
	  dif.m_ang1 = dif.findAng1();		// call findAng1 to find new first angle
	  dif.m_mag = dif.findMag();		// call findMag to find new magnitude
	  dif.m_ang2 = dif.findAng2();		// call findAng2 to find new second angle
	  return dif;				// return dif Vector3D
  }

  /** dotProduct
   *  Returns the dot product of this Vector3D and the given Vector3D.
   */
  public float dotProduct(Vector3D other) {
	  float dotProd = (this.getX() * other.getX()); // first part of dotProd operation
	  dotProd += (this.getY() * other.getY());	// second part of dotProd operation
	  dotProd += (this.getZ() * other.getZ());	// third part of dotProd operation
	  return dotProd;				// return dotProd float
  }

  /** crossProduct
   *  Returns the cross product of this Vector3D and the given Vector3D.
   */
  public Vector3D crossProduct(Vector3D other) {
	  Vector3D crossProd = new Vector3D();		 // let crossProd = some new Vector3D
	  crossProd.m_x = (this.getY() * other.getZ());  // x = (y1 * z2)
	  crossProd.m_x -= (other.getY() * this.getZ()); // x = (y1 * z2) - (y2 * z1)
	  crossProd.m_y = (other.getX() * this.getZ());  // y = (x2 * z1)
	  crossProd.m_y -= (this.getX() * other.getZ()); // y = (x2 * z1) - (x1 * z2)
	  crossProd.m_z = (this.getX() * other.getY());  // z = (x1 * y2)
	  crossProd.m_z -= (other.getX() * this.getY()); // z = (x1 * y2) - (x2 * y1)
	  crossProd.m_ang1 = crossProd.findAng1();	 // call findAng1 to find the new first angle
	  crossProd.m_mag = crossProd.findMag();	 // call findMAg to find the new magnitude
	  crossProd.m_ang2 = crossProd.findAng2();	 // call findAng2 to find the new second angle
	  return crossProd;				 // return crossProd Vector3D
  }

  /** scalarMultiply
   *  Returns this Vector3D scaled by the given scalar.
   */
  public Vector3D scalarMultiply(float scalar) {
	  Vector3D prod = new Vector3D(this);	// set new Vector3D = operand so operand won't change
	  prod.m_x *= scalar;			// multiply y by scalar
	  prod.m_y *= scalar;			// multiply x by scalar
	  prod.m_z *= scalar;			// multiply z by scalar
	  prod.m_mag = prod.findMag();		// call findMag to find new magnitude
	  return prod;				// return prod Vector3D
  }

  /** normalize
   *  Returns the normalized version of this Vector3D, a Vector3D with the same
   *    angles with magnitude 1.
   */
  public Vector3D normalize() {
	  Vector3D norm = new Vector3D(this);	// set new Vector3D = operand so operand won't change
	  norm.m_x /= this.getMagnitude();	// divide x by magnitude
	  norm.m_y /= this.getMagnitude();	// divide y by magnitude
	  norm.m_z /= this.getMagnitude();	// divide z by magnitude
	  norm.m_mag = norm.findMag();		// call findMag to find new magnitude
	  return norm;				// return norm Vector3D
  }

  // Additional method
  /** angleBetween
   *  Returns the angle (in radians) between this Vector3D and the other Vector3D
   */
  public float angleBetween(Vector3D other) {
          double radians;                       // declare radians for result of arccos()
          double num = this.dotProduct(other);  // numerator = dot product of both vectors
          double den = this.getMagnitude();
          den *= other.getMagnitude();		// denominator = product of both magnitudes
          if (den != 0.0) {                     // error check for divide by 0 error
                radians = Math.acos(num/den);   // set radians = arccos(num/den) to find angle
          } else { radians = 0; }               // if num = 0, radians = 0
          return ((float) radians);             // returns radians
  } 



  // Manipulation functions
  // None.  Vectors are immutable.
  
  


  // Additional functions

  // Function to print the attributes of a 3DVector
  public void print() {
	  float x = this.getX();
	  float y = this.getY();
	  float z = this.getZ();
	  float ang1 = this.getAngle1();
	  float ang2 = this.getAngle2();
	  float mag = this.getMagnitude();
	  System.out.printf("The 3D vector (%.3f, %.3f, %.3f) ", x, y, z);
	  System.out.printf("has a first angle of%n%.3f radians, ", ang1);
	  System.out.printf("a second angle of %.3f radians, ", ang2);
	  System.out.printf("and %na magnitude of %.3f units.", mag);
  }

  // Function to find the first angle of a 3DVector
  public float findAng1() {
	  double radians;				       // declare radians for result of arctan()
	  if (this.getX() != 0.0) {			       // error check for divide by 0 error
	  	radians = Math.atan(this.getY()/this.getX());  // set radians = arctan(y/x) to find angle in radians
	  } else { radians = 0; }			       // if x = 0, radians = 0
	  return ((float) radians);			       // returns radians
  }

  // Function to find the second angle of a 3DVector
  public float findAng2() {
	  double radians;			// declare radians for result of arccos()
	  double value;				// declare value for operations
	  double mag = this.getMagnitude();	// declare mag for operations
	  if (mag != 0.0) {			// error check for divide by 0 error
		  value = (this.getZ() / mag);	// set value = z / mag
	  } else { value = 0; }			// if mag = 0, value = 0
	  if (value == 0.0) { return 0f; }	// if value = 0, return 0
	  else {
	  	radians = Math.acos(value);	// set radians = arccos(z/mag) to find angle in radians
	  } return ((float) radians);		// return radians
  }

  // Function to find the magnitude given an x,y,z argument
  public float findMag() {
	  double length = Math.pow(this.getX(),2); // first part of finding length (magnitude^2)
	  length += Math.pow(this.getY(),2);	   // second part of finding length (magnitude^2)
	  length += Math.pow(this.getZ(),2);	   // third part of finding length (magnitude^2)
	  length = Math.sqrt(length);		   // take square root of magnitude^2
	  return ((float) length);		   // return length (magnitude)
  }

}
