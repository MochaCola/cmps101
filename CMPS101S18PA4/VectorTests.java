public class VectorTests {
	
	// Note: All printed values of vectors will be rounded to the nearest thousandth.
	// See the file "ExpectedResults" for a list of correct values for the following operations.

	public static void main(String args[]) {
		System.out.println("---------------------------------------------------------------------");
		System.out.println("                          2D Vector Tests                            ");
		System.out.println("---------------------------------------------------------------------");

		Vector vec0 = new Vector();
                Vector vec1 = new Vector(3f, 4f);
                Vector vec2 = new Vector(5f, 12f);
                Vector vec3 = Vector.polarVector(0.965f, 281f);
		Vector vec4 = vec3.add(vec2);
		Vector vec5 = vec2.subtract(vec1);
		Vector vec6 = vec1.crossProduct(vec2);
		float prod1 = vec1.dotProduct(vec2);
		Vector vec7 = vec1.normalize();
		float ang1 = vec1.angleBetween(vec2);
		Vector vec8 = vec2.scalarMultiply(10f);

		System.out.println("                       Default Constructor Test                      \n");
		vec0.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("                  X/Y Constructor Test (x = 3, y = 4)                \n");
		vec1.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("                  X/Y Constructor Test (x = 5, y = 12)               \n");
		vec2.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("          Polar Constructor Test (ang = 0.965, magnitude = 281)      \n");
		vec3.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("                      Adding (5, 12) to (160, 231)                   \n");
		vec4.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("                     Subtracting (3, 4) from (5, 12)                 \n");
		vec5.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("              Taking Cross Product of (3, 4) and (5, 12)             \n");
		vec6.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("               Taking Dot Product of (3, 4) and (5, 12)              \n");
		System.out.println("The dot product of (3, 4) and (5, 12) is " + prod1 + ".");
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("                         Normalizing (3, 4)                          \n");
		vec7.print();
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("            Finding the Angle Between (3, 4) and (5, 12)             \n");
		System.out.printf("The angle between (3, 4) and (5, 12) is %.3f radians.%n", ang1);
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("            Scalar Multiplying (5, 12) by the Scalar 10              \n");
		vec8.print();
		System.out.println("\n---------------------------------------------------------------------");



		System.out.println("---------------------------------------------------------------------");
                System.out.println("                          3D Vector Tests                            ");
                System.out.println("---------------------------------------------------------------------");

                Vector3D vec3D_0 = new Vector3D();
                Vector3D vec3D_1 = new Vector3D(3f, 4f, 5f);
                Vector3D vec3D_2 = new Vector3D(5f, 12f, 13f);
		Vector3D vec3D_3 = Vector3D.polarVector(0.785f, 0.955f, 1.732f);
                Vector3D vec3D_4 = vec3D_3.add(vec3D_2);
		Vector3D vec3D_5 = vec3D_2.subtract(vec3D_1);
                Vector3D vec3D_6 = vec3D_1.crossProduct(vec3D_2);
                float prod2 = vec3D_1.dotProduct(vec3D_2);
                Vector3D vec3D_7 = vec3D_1.normalize();
                float ang2 = vec3D_1.angleBetween(vec3D_2);
                Vector3D vec3D_8 = vec3D_2.scalarMultiply(10f);

                System.out.println("                       Default Constructor Test                      \n");
                vec3D_0.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("               XYZ Constructor Test (x = 3, y = 4, z = 5)            \n");
                vec3D_1.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("              XYZ Constructor Test (x = 5, y = 12, z = 13)           \n");
                vec3D_2.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println(" Polar Constructor Test (ang1 = 0.785, ang2 = 0.955, mag = sqrt(3))  \n");
                vec3D_3.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("                    Adding (5, 12, 13) to (1, 1, 1)                  \n");
                vec3D_4.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("                Subtracting (3, 4, 5) from (5, 12, 13)               \n");
                vec3D_5.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("           Taking Cross Product of (3, 4, 5) and (5, 12, 13)         \n");
                vec3D_6.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("            Taking Dot Product of (3, 4, 5) and (5, 12, 13)          \n");
                System.out.println("The dot product of (3, 4, 5) and (5, 12, 13) is " + prod2 + ".");
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("                       Normalizing (3, 4, 5)                         \n");
                vec3D_7.print();
                System.out.println("\n---------------------------------------------------------------------");
                System.out.println("          Finding the Angle Between (3, 4, 5) and (5, 12, 13)        \n");
                System.out.printf("The angle between (3, 4, 5) and (5, 12, 13) is %.3f radians.%n", ang2);
		System.out.println("\n---------------------------------------------------------------------");
		System.out.println("            Scalar Multiplying (5, 12, 13) by the Scalar 10          \n");
		vec3D_8.print();
		System.out.println("\n---------------------------------------------------------------------");
	}
}
