import java.lang.Math;

// Vector.java
// A class that implements the Vector ADT.
//
// For this assignment, you must complete this code skeleton.
// You may not change the function prototypes.
// You are expected to fill in the functions to make them work
// as expected, and you can add as much as you need or want.
// We recommend implementing the Vector ADT using x and y coordinates.

// Notes:
// Angles are always in radians, not degrees.

class Vector {

  // Fields
  
  private float m_x;	// x field
  private float m_y;	// y field
  private float m_ang;	// angle field (in radians)
  private float m_mag;	// magnitude field

  // Constructors

  // The default constructor should create a new Vector with no magnitude.
  public Vector() {
	  m_x = m_y = 0;	     // set x & y to 0
	  m_ang = this.findAng();    // because x & y are 0, findAng will return 0
	  m_mag = this.findMag();    // because x & y are 0, findMag will return 0
  }

  // This constructor takes an x and a y coordinate for the Vector.
  public Vector(float x, float y) {
	  m_x = x;		     // set m_x field to x argument
	  m_y = y;		     // set m_y field to y argument
	  m_ang = this.findAng();    // call findAng to find angle
	  m_mag = this.findMag();    // call findMag to find magnitude
  }

  // Additional constructor - creates a Vector from another Vector
  public Vector(Vector other) {
	  m_x = other.getX();		// m_x = other.m_x
	  m_y = other.getY();		// m_y = other.m_y
	  m_ang = other.getAngle();	// m_ang = other.m_ang
	  m_mag = other.getMagnitude(); // m_mag = other.m_mag
  }

  // This "constructor" takes an angle and a magnitude for the Vector.
  // It is not a traditional constructor because only one function can have
  //   the signature Vector(float, float).
  public static Vector polarVector(float angle, float magnitude) {
	  Vector temp = new Vector();
	  temp.m_x = magnitude * ((float) Math.cos(angle)); // x = rcos(theta)
	  temp.m_y = magnitude * ((float) Math.sin(angle)); // y = rsin(theta)
	  temp.m_ang = angle;				    // angle will be the same
	  temp.m_mag = magnitude;			    // magnitude will be the same
	  return temp;					    // return temp Vector
  }



  // Access functions

  /** getX
   *  Returns the x coordinate of the Vector.
   */
  public float getX() {
	  return m_x; // return m_x field
  }

  /** getY
   *  Returns the y coordinate of the Vector.
   */
  public float getY() {
	  return m_y; // return m_y field
  }

  /** getAngle
   *  Returns the angle of the Vector.
   */
  public float getAngle() {
	  return m_ang; // return m_ang field
  }

  /** getMagnitude
   *  Returns the magnitude of the Vector.
   */
  public float getMagnitude() {
	  return m_mag; // return m_mag field
  }

  /** add
   *  Returns the sum of this Vector with the given Vector.
   */
  public Vector add(Vector other) {
	  Vector sum = new Vector(this);	// set new Vector = operand so operand won't change
	  sum.m_x += other.getX();		// add x to other x
	  sum.m_y += other.getY();		// add y to other y
	  sum.m_ang = sum.findAng();		// call findAng to find new angle
	  sum.m_mag = sum.findMag();		// call findMag to find new magnitude
	  return sum;				// return sum Vector
  }

  /** subtract
   *  Returns the difference between this Vector and the given Vector.
   */
  public Vector subtract(Vector other) {
	  Vector dif = new Vector(this);	// set new Vector = operand so operand won't change
	  dif.m_x -= other.getX();		// subtract x by other x
	  dif.m_y -= other.getY();		// subtract y by other x
	  dif.m_ang = dif.findAng();		// call findAng to find new angle
	  dif.m_mag = dif.findMag();		// call findMag to find new magnitude
	  return dif;				// return dif Vector
  }

  /** dotProduct
   *  Returns the dot product of this Vector and the given Vector.
   */
  public float dotProduct(Vector other) {
	  float dotProd = (this.getX() * other.getX()); // first half of dotProd operation
	  dotProd += (this.getY() * other.getY());	// second half of dotProd operation
	  return dotProd;				// return dotProd float
  }

  // Additional method
  /** crossProduct
   *  Returns the cross product of this Vector and the given Vector.
   */
  public Vector crossProduct(Vector other) {
	  Vector crossProd = new Vector();		 // new Vector
	  crossProd.m_x = (this.getX() * other.getY());  // x = x1 * y2
	  crossProd.m_x -= (other.getX() * this.getY()); // x = (x1 * y2) - (x2 * y1)
	  crossProd.m_y = (other.getX() * this.getY());  // y = x2 * y1  
	  crossProd.m_y -= (this.getX() * other.getY()); // y = (x2 * y1) - (x1 * y2)
	  crossProd.m_ang = crossProd.findAng();	 // call findAng to find new angle
	  crossProd.m_mag = crossProd.findMag();	 // call findMag to find new magnitude
	  return crossProd;				 // return crossProd Vector
  }

  /** scalarMultiply
   *  Returns this Vector scaled by the given scalar.
   */
  public Vector scalarMultiply(float scalar) {
	  Vector prod = new Vector(this);	// set new Vector = operand so operand won't change
	  prod.m_x *= scalar;			// multiply y by scalar
	  prod.m_y *= scalar;			// multiply x by scalar
	  prod.m_mag = prod.findMag();		// call findMag to find new magnitude
	  return prod;				// return prod Vector
  }

  /** normalize
   *  Returns the normalized version of this Vector, a Vector with the same
   *    angle with magnitude 1.
   */
  public Vector normalize() {
	  Vector norm = new Vector(this);	// set new Vector = operand so operand won't change
	  norm.m_x /= this.getMagnitude();	// divide x by magnitude
	  norm.m_y /= this.getMagnitude();	// divide y by magnitude
	  norm.m_mag = norm.findMag();		// call findMag to find new magnitude
	  return norm;				// return norm Vector
  }

  // Additional method
  /** angleBetween
   *  Returns the angle (in radians) between this Vector and the other Vector
   */
  public float angleBetween(Vector other) {
	  double radians;			// declare radians for result of arccos()
	  double num = this.dotProduct(other);	// numerator = dot product of both vectors
	  double den = this.getMagnitude();
	  den *= other.getMagnitude();		// denominator = product of both magnitudes
	  if (den != 0.0) {			// error check for divide by 0 error
	  	radians = Math.acos(num/den);	// set radians = arccos(num/den) to find angle
	  } else { radians = 0; }		// if num = 0, radians = 0
	  return ((float) radians);		// returns radians
  }



  // Manipulation functions
  // None.  Vectors are immutable.
  
  

  // Some other additional functions

  // Function to print the attributes of a Vector
  public void print() {
	  float x = this.getX();
	  float y = this.getY();
	  float ang = this.getAngle();
	  float mag = this.getMagnitude();
	  System.out.printf("The 2D vector (%.3f, %.3f) has ", x, y);
	  System.out.printf("an angle of %.3f radians, and%n", ang);
	  System.out.printf("a magnitude of %.3f units.", mag);
  }
  
  // Function to find the angle of a Vector
  public float findAng() {
	  double radians;			// declare radians for result of arctan()
	  float x = this.getX();		// let x = this.getX();
	  float y = this.getY();		// let y = this.getY();
	  if (x != 0.0) {			// error check for divide by 0 error
	  	radians = Math.atan(y/x);	// set radians = arctan(x/y) to find angle
	  } else { radians = 0; }		// if x = 0, radians = 0
	  return ((float) radians);		// returns radians
  }

  // Function to find the magnitude of a Vector
  public float findMag() {
	  double length = Math.pow(this.getX(),2);	// first part of finding length (magnitude^2)
	  length += Math.pow(this.getY(),2);		// second part of finding length (magnitude^2)
	  length = Math.sqrt(length);			// take square root of magnitude^2
	  return ((float) length);			// return length (magnitude)
  }

}
