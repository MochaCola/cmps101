Script started on 2019-02-14 08:45:38-0800
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ pws[Kd
/home/kenny/CMPS101/CMPS101S18PA2
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ ls -l
total 52
-rw-r--r-- 1 kenny kenny  9136 Feb 14 08:36 apint.c
-rw-r--r-- 1 kenny kenny   635 Feb 13 22:23 apint.h
-rw-r--r-- 1 kenny kenny  1346 Feb 13 18:55 helpfulMethods.h
-rwxr-xr-x 1 kenny kenny 17664 Feb 14 08:44 [0m[01;32mmain[0m
-rw-r--r-- 1 kenny kenny  3994 Feb 14 08:38 main.c
-rw-r--r-- 1 kenny kenny  1034 Feb 13 19:37 NoteToGrader
-rw-r--r-- 1 kenny kenny     0 Feb 14 08:45 pa2submissionfile.txt
-rw-r--r-- 1 kenny kenny   655 Feb 13 19:22 README
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat README
Directory path: /home/kenny/CMPS101/CMPS101S18PA2

Directory files:

	Source files:
	
		apint.c - 		The C file containing all of the implementations for my apint class methods.
		apint.h - 		The header file containing all of the method prototypes. 
		helpfulMethods.h - 	A header file I created containing useful methods that I invoke throughout my code.

	Test files:

		main.c - 		The test file containing all of the testing code.
		main - 			The location where the results of main.c are output.

	Other files:

		README - 		This file, documenting all of the files in this directory.
		NoteToGrader -		A short note to the grader describing my approach.
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat NoteToGrader
         -         -        -        -         -         -         -         -         -         -
I'm sorry about this late submission! Between having two midterms this last week and getting
awfully sick, I found myself pressed for the time and energy required to see this program through
to completion by the due date. I hope what I have accomplishes everything you're hoping to see.
I haven't coded in C before, so I found this program to be exceptionally challenging.

I chose to store the data for the apint variable in an array of integers, rather than longs, like
I did in the previous program. I found that storing individual numbers was much easier to work
with, and a lot less problematic.

Also, I hope that the comments I included in my files are of benefit to you when looking
through my work. I tried to make it neat and easy to follow. I went to the effort of including
what the correct results of my tests should be, that way you don't have to manually check them
yourself. I hope that helps.

Best wishes,
Kenny Blum
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat apint.c
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "helpfulMethods.h"
#include "apint.h"

typedef struct apint {	
        int *m_value;
        int size;
} apint;

// access procedures
void 	Print(apintHandle value) 			// print form: "m_value = #\n"
{
	printf("m_value = ");				// print preliminary statement
	for (int i = 0; i < value->size; i++) {		// iterate through m_value array elements
		printf("%d", value->m_value[i]);	// print each one
	} printf("\n");					// print newline escape sequence
}
int	First(apintHandle value)			// first element function
{
	return value->m_value[0];			// returns first element in m_value
}
void	ToString(apintHandle value, char **str)		// string conversion function
{
	int bump = 0;
	if (First(value) < 0) {
		(*str) = calloc(value->size + 1, sizeof(char));
		bump = 1;
	} else {
		(*str) = calloc(value->size, sizeof(char));	// allocate size of string
	} for (int i = 0; i < value->size; i++) {		// iterate through each element
		if (i == 0 && First(value) < 0) {
			(*str)[i] = '-';
			(*str)[i+bump] = (-1 * value->m_value[i]) + '0';		// insert character conversion of int into string
		} else {
			(*str)[i+bump] = value->m_value[i] + '0';
		}
	}
}

// manipulation procedures
void	Trim(apintHandle *value)			// trims zeros from the front of m_value
{
	printf("trim reached");
	int size = (*value)->size;
	int counter = 0;
	int numZeros = 0;
	char *result;
	while ((*value)->m_value[counter] == 0) {
		numZeros++;
		counter++;
	} if (numZeros == size - 1) {
		Constructor(&(*value));
		return;
	} else {
		result = calloc(size - numZeros, sizeof(char));
		for (int i = numZeros; i < size; i++) {
			result[i - numZeros] = (*value)->m_value[i] + '0';
		} String_constructor(&(*value), result);
	}
}
void	MatchFill(apintHandle *value1, apintHandle value2)	// fills m_value with zeros to match length
{								// of other m_value
	int fill = value2->size - (*value1)->size;
	int filler[fill];
	memset(filler, 0, sizeof(filler));
	int newArray[value2->size];
	for (int i = 0; i < value2->size; i++) {
		if (i < fill) { newArray[i] = filler[i]; }
		else if (i >= fill) { newArray[i] = (*value1)->m_value[i - fill]; }
	} (*value1)->m_value = realloc((*value1)->m_value, value2->size * sizeof(int));
	(*value1)->size = value2->size;
	memcpy((*value1)->m_value, newArray, sizeof(newArray));
}
void 	Add(apintHandle *value1, apintHandle value2)		// addition function
{
	int size1 = (*value1)->size;
	int size2 = value2->size;
	int largestSize;
	if (First((*value1)) < 0 && First(value2) >= 0) {
		char *temp;
		ToString((*value1), &temp);
		substring(&temp, 1, strlen(temp));
		String_constructor(&(*value1), temp);
		Subtract(&value2, (*value1));
		ToString(value2, &temp);
		String_constructor(&(*value1), temp);
		return;
	} if (First((*value1)) >= 0 && First(value2) < 0) {
		char *temp;
                ToString(value2, &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&value2, temp);
                Subtract(&(*value1), value2);
                return;
	} if (size1 >= size2) { 
		largestSize = size1;
		if (size1 != size2) {
			MatchFill(&value2, (*value1));
		}
	} if (size1 < size2) { 
		largestSize = size2;
		MatchFill(&(*value1), value2);
	} int temp[largestSize];
	int rem = 0;
	for (int i = largestSize-1; i >= 0; i--) {
		int v1 = (*value1)->m_value[i];
		int v2 = value2->m_value[i];
		int val = rem + v1 + v2;
		if (val >= 10) {
			rem = 1;
			val -= 10;
		} else { rem = 0; }
		temp[i] = val;
	}
	(*value1)->m_value = realloc((*value1)->m_value, largestSize * sizeof(int));
	(*value1)->size = largestSize;
        memcpy((*value1)->m_value, temp, sizeof(temp));
	if (First((*value1)) == 0) {
		Trim(&(*value1));
	}
}
void 	Subtract(apintHandle *value1, apintHandle value2)	// subtraction function
{
	int size1 = (*value1)->size;
        int size2 = value2->size;
        int largestSize;
	if (First((*value1)) < 0 && First(value2) >= 0) {
                char *temp;
                ToString((*value1), &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&(*value1), temp);
                Add(&value2, (*value1));
                ToString(value2, &temp);
                String_constructor(&(*value1), temp);
		(*value1)->m_value[0] = First((*value1)) * -1;
                return;
        } if (First((*value1)) >= 0 && First(value2) < 0) {
                char *temp;
                ToString(value2, &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&value2, temp);
                Add(&(*value1), value2);
                return;
	} if (size1 >= size2) {
		if (memcmp((*value1), value2, size1) == 0) {
			Constructor(&(*value1));
			return;
		} largestSize = size1;
                if (size1 != size2) {
                        MatchFill(&value2, (*value1));
                }
        } if (size1 < size2) {
		if (memcmp(value2, (*value1), size1) > 0) {
			char *temp;
			ToString(value2, &temp);
			apintHandle apintTemp;
			String_constructor(&apintTemp, temp);
			Subtract(&apintTemp, (*value1));
			ToString(apintTemp, &temp);
			String_constructor(&(*value1), temp);
			(*value1)->m_value[0] = First((*value1)) * -1;
			return;
		}
                largestSize = size2;
                MatchFill(&(*value1), value2);
        }
        int temp[largestSize];
        int rem = 0;
        for (int i = largestSize-1; i >= 0; i--) {
                int v1 = (*value1)->m_value[i];
                int v2 = value2->m_value[i];
                int val = rem + (v1 - v2);
                if (val < 0) {
                        rem = -1;
                        val += 10;
                } else { rem = 0; }
                temp[i] = val;
        }
        (*value1)->m_value = realloc((*value1)->m_value, largestSize * sizeof(int));
        (*value1)->size = largestSize;
        memcpy((*value1)->m_value, temp, sizeof(temp));
	if (First((*value1)) == 0) {
                Trim(&(*value1));
        }
}
void 	Multiply(apintHandle *value1, apintHandle value2)	// multiplication function
{
	char *str1;
	char *str2;
	char *result;
	ToString((*value1), &str1);
	ToString(value2, &str2);
	int size = strlen(str2);
	if (size <= 1) {
		if (size == 0) {
			Constructor(&(*value1));
			return;
		} else {
			int num = str2[0] - '0';
			int val = 0;
			int rem = 0;
			char temp[1];
			for (int i = strlen(str1) - 1; i >= 0; i--) {
				val = rem + (str1[i] - '0') * num;
				rem = 0;
				if (val >= 10 && i != 0) {
					while (val >= 10) {
						val -= 10;
						rem += 1;
					}
				} temp[0] = val + '0';
				result = calloc(strlen(temp), sizeof(char));
				strcat(temp, result);
				strcpy(result, temp);
			} String_constructor(&(*value1), result);
		} return;
	} else {
		apintHandle product;
		Constructor(&product);
		for (int i = 0; i < size; i++) {
			int num = str2[i] - '0';
			char* temp;
			apintHandle apintNum;
			apintHandle apintRes;
			Int_constructor(&apintNum, num);
			Multiply(&(*value1), apintNum);
			ToString((*value1), &result);
			for (int j = size - i - 1; j > 0; j--) {
				strcat(result, "0");
			} String_constructor(&apintRes, result);
			Add(&product, apintRes);
		}
	}
}


// creation procedures
void 	Constructor(apintHandle *value)
{
	(*value) = malloc(sizeof(apint));		// allocate apint value to the size of an apint (16 bytes)
        (*value)->m_value = calloc(1, sizeof(int));     // allocate m_value c-string to one int element
        (*value)->m_value[0] = 0;                       // set that one int element equal to 0
        (*value)->size = 1;                             // set size to one to represent one element
}
void 	String_constructor(apintHandle *value, char str[])
{
	int strLen = strlen(str);				// size variable to be used throughout method
	int polarity = 1;					// variable to represent positivity or negativity
	(*value) = malloc(sizeof(apint));			// allocate apint value to the size of an apint (16 bytes)
	if (str[0] == '-' || str[0] == '+') {			// if str char is '-' or '+'...
		if (str[0] == '-') { polarity = -1; }		// if '-', set polarity to -1
		substring(&str, 1, strLen);			// remove the '-' or '+' from str[] using substring method
		strLen--;					// decrement strLen by 1 due to this change
	}
	(*value)->m_value = calloc(strLen, sizeof(int));	// allocate strLen int elements
	(*value)->size = strLen;				// set size to strLen to represent strLen elements
	for (int i = 0; i < strLen; i++) {			// iterate through each string character
		int strVal = str[i] - '0';			// variable to represent integer conversion of string character
		(*value)->m_value[i] = polarity * strVal;	// insert value of polarity * strVal into m_value
		if (polarity == -1) { polarity = 1; }		// polarity of -1 should only affect the first character of m_value
	}
}
void 	Int_constructor(apintHandle *value, int integer)
{
	char str[10];						// declare string of length at most 10 (because range of int is 10)
	sprintf(str, "%d", integer);				// convert integer to string with sprintf()
	String_constructor(&(*value), str);			// pass string and apint to string constructor
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat apinth[Kh[K.h
#ifndef APINT_H
#define APINT_H
typedef struct apint *apintHandle;

// access procedures
void 	Print(apintHandle value);
int	First(apintHandle value);
void	ToString(apintHandle value, char **str);

// manipulation procedures
void	MatchFill(apintHandle *value1, apintHandle value2);
void 	Add(apintHandle *value1, apintHandle value2);
void 	Subtract(apintHandle *value1, apintHandle value2);
void 	Multiply(apintHandle *value1, apintHandle value2);

// creation procedures
void 	Constructor(apintHandle *value);
void 	String_constructor(apintHandle *value, char string[]);
void	Int_constructor(apintHandle *value, int integer);

#endif
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat helpfulMethods.h
#ifndef HELPFULMETHODS_H
#define HELPFULMETHODS_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void substring(char *str[], int start, int end) // substring of str[] from indices start to end
{
        int i = 0;				// variable to represent iteration
	int length = end - start;		// variable to represent resulting size of array
	char temp[length];			// temporary array to create substring
        while (i < length) {			// iteration from 0 to length
                temp[i] = (*str)[start + i];	// insert character at str[start + i] into temp[]
                i++;				// increment i for next iteration
        } temp[i] = '\0';			// after loop, add '\0' to represent end of string
	(*str) = malloc(length);		// allocate appropriate memory for str[]
	strcpy((*str), temp);			// copy temp[] into str[]
}
void printStr(char *str[])			// prints entirety of a c-string
{
	int size = strlen((*str));		// size variable to be used in loop
	for (int i = 0; i < size; i++) {	// iterates through
		printf("%c", (*str)[i]);	// prints each individual character
	} printf("\n");				// at the end, new line
}
void printVal(int val[], int size) 		// prints value of an int array
{
	for (int i = 0; i < size; i++) {	// iterates through
		printf("%d", val[i]);		// prints each individual integer
	} printf("\n");				// at the end, new line
}
#endif
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat main.c
#include <stdio.h>
#include <stdlib.h>
#include "apint.h"

void reconstruct1(apintHandle *test1, apintHandle *test2); 
void reconstruct2(apintHandle *test3, apintHandle *test4);
void reconstruct3(apintHandle *test5, apintHandle *test6);

int main(void) {
	apintHandle test1;
        apintHandle test2;
        apintHandle test3;
        apintHandle test4;
	apintHandle test5;
	apintHandle test6;
	
	// Constructor Tests
	printf("Constructor Tests:\n-----------------------\n");
	printf("Default constructor: (correct result: 0)\n");
	Constructor(&test1);
	Print(test1);
	printf("Integer constructor: (correct result: 1223334444)\n");
	Int_constructor(&test2, 1223334444);
	Print(test2);
	printf("String constructor, no sign: (correct result: 12345678901234567890)\n");
	String_constructor(&test3, "12345678901234567890");
	Print(test3);
	printf("String constructor, with '-' : (correct result: -12345678901234567890)\n");
	String_constructor(&test4, "-12345678901234567890");
	Print(test4);
	printf("String constructor: (correct result: -6666664444)\n");
	String_constructor(&test5, "-6666664444");
	Print(test5);
	printf("Integer constructor: (correct result: -1337)\n");
	Int_constructor(&test6, -1337);
	Print(test6);

	// Addition Tests
	printf("\n\nAddition Tests:\n-----------------------\n");
	printf("Adding + to +, same lengths: (correct result: 24691357802469135780)\n");
	Add(&test3, test3);
	Print(test3);
	printf("Adding + to +, len(val1) > len(val2): (correct result: 24691357803692470224)\n");
	Add(&test3, test2);
	Print(test3);
	printf("Adding + to +, len(val1) < len(val2): (correct result: 24691357804915804668)\n");
	Add(&test2, test3);
	Print(test2);
	printf("Adding - to +, same lengths: (correct result: 12345678902457902334)\n");
	Add(&test4, test3);
	Print(test4);
	printf("Adding + to -, len(val1) > len(val2): (correct result: 24691357798249140224)\n");
	Add(&test2, test5);
	Print(test2);
	printf("Adding - to -, len(val1) < len(val2): (correct result: -6666663781)\n");
	Add(&test6, test5);
	Print(test6);
	
	reconstruct1(&test1, &test2);
	reconstruct2(&test3, &test4);
	reconstruct3(&test5, &test6);
	
	// Subtraction Tests
	printf("\n\nSubtraction Tests:\n-----------------------\n");
	printf("Subtracting + from +, same lengths: (correct result: 0)\n");
        Subtract(&test3, test3);
        Print(test3);
        printf("Subtracting + from 0, len(val1) < len(val2): (correct result: -1223334444)\n");
        Subtract(&test3, test2);
        Print(test3);
        printf("Subtracting - from +, same lengths: (correct result: 2446668888)\n");
        Subtract(&test2, test3);
        Print(test2);
        printf("Subtracting + from -, len(val1) > len(val2): (correct result: -12345678903681236778)\n");
        Subtract(&test4, test2);
        Print(test4);
        printf("Subtracting - from +, len(val1) > len(val2): (correct result: 12345678910347901222)\n");
        Subtract(&test2, test5);
        Print(test2);
	printf("Subtracting - from -, same lengths: (correct result: 5333334893)\n");
	Subtract(&test6, test5);
        Print(test6);

	reconstruct1(&test1, &test2);
        reconstruct2(&test3, &test4);
        reconstruct3(&test5, &test6);

	// Multiplication Tests
	printf("\n\nDespite my best efforts... I couldn't get multiplication to work as I intended.\n");
	printf("Multiplication Tests:\n-----------------------\n");
	printf("Multiplying + by +, val1 > val2: (correct result: 15102894234444321023293403160)\n");
	Multiply(&test2, test3);
	Print(test2);
	return 0;
}

void reconstruct1(apintHandle *test1, apintHandle *test2)
{
	Constructor(&(*test1));
        Int_constructor(&(*test2), 1223334444);
}	
void reconstruct2(apintHandle *test3, apintHandle *test4)
{
	String_constructor(&(*test3), "12345678901234567890");
        String_constructor(&(*test4), "-12345678901234567890");
}
void reconstruct3(apintHandle *test5, apintHandle *test6)
{
        String_constructor(&(*test5), "-6666664444");
	Int_constructor(&(*test6), -1337);
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ cat main.chelpfulMethods.h[9Papint.hcNoteToGrader[6PREADME[5Pls -l[2Ppwdexitcat main.c[5Pls -l./maingcc apint.c main.c -o main[6Pcat helpfulMethods.hgcc apint.c main.c -o main
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ ./main
Constructor Tests:
-----------------------
Default constructor: (correct result: 0)
m_value = 0
Integer constructor: (correct result: 1223334444)
m_value = 1223334444
String constructor, no sign: (correct result: 12345678901234567890)
m_value = 12345678901234567890
String constructor, with '-' : (correct result: -12345678901234567890)
m_value = -12345678901234567890
String constructor: (correct result: -6666664444)
m_value = -6666664444
Integer constructor: (correct result: -1337)
m_value = -1337


Addition Tests:
-----------------------
Adding + to +, same lengths: (correct result: 24691357802469135780)
m_value = 24691357802469135780
Adding + to +, len(val1) > len(val2): (correct result: 24691357803692470224)
m_value = 24691357803692470224
Adding + to +, len(val1) < len(val2): (correct result: 24691357804915804668)
m_value = 24691357804915804668
Adding - to +, same lengths: (correct result: 12345678902457902334)
m_value = 12345678902457902334
Adding + to -, len(val1) > len(val2): (correct result: 24691357798249140224)
m_value = 24691357798249140224
Adding - to -, len(val1) < len(val2): (correct result: -6666663781)
m_value = -6666663781


Subtraction Tests:
-----------------------
Subtracting + from +, same lengths: (correct result: 0)
m_value = 0
Subtracting + from 0, len(val1) < len(val2): (correct result: -1223334444)
m_value = -1223334444
Subtracting - from +, same lengths: (correct result: 2446668888)
m_value = 2446668888
Subtracting + from -, len(val1) > len(val2): (correct result: -12345678903681236778)
m_value = -12345678903681236778
Subtracting - from +, len(val1) > len(val2): (correct result: 12345678910347901222)
m_value = 12345678910347901222
Subtracting - from -, same lengths: (correct result: 5333334893)
m_value = 5333334893


Despite my best efforts... I couldn't get multiplication to work as I intended.
Multiplication Tests:
-----------------------
Multiplying + by +, val1 > val2: (correct result: 15102894234444321023293403160)
m_value = 0
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ ls -l
total 72
-rw-r--r-- 1 kenny kenny  9136 Feb 14 08:36 apint.c
-rw-r--r-- 1 kenny kenny   635 Feb 13 22:23 apint.h
-rw-r--r-- 1 kenny kenny  1346 Feb 13 18:55 helpfulMethods.h
-rwxr-xr-x 1 kenny kenny 17664 Feb 14 08:46 [0m[01;32mmain[0m
-rw-r--r-- 1 kenny kenny  3994 Feb 14 08:38 main.c
-rw-r--r-- 1 kenny kenny  1034 Feb 13 19:37 NoteToGrader
-rw-r--r-- 1 kenny kenny 20480 Feb 14 08:46 pa2submissionfile.txt
-rw-r--r-- 1 kenny kenny   655 Feb 13 19:22 README
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA2[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA2[00m$ exit
exit

Script done on 2019-02-14 08:46:52-0800
