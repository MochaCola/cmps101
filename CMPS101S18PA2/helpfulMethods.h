#ifndef HELPFULMETHODS_H
#define HELPFULMETHODS_H
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void substring(char *str[], int start, int end) // substring of str[] from indices start to end
{
        int i = 0;				// variable to represent iteration
	int length = end - start;		// variable to represent resulting size of array
	char temp[length];			// temporary array to create substring
        while (i < length) {			// iteration from 0 to length
                temp[i] = (*str)[start + i];	// insert character at str[start + i] into temp[]
                i++;				// increment i for next iteration
        } temp[i] = '\0';			// after loop, add '\0' to represent end of string
	(*str) = malloc(length);		// allocate appropriate memory for str[]
	strcpy((*str), temp);			// copy temp[] into str[]
}
void printStr(char *str[])			// prints entirety of a c-string
{
	int size = strlen((*str));		// size variable to be used in loop
	for (int i = 0; i < size; i++) {	// iterates through
		printf("%c", (*str)[i]);	// prints each individual character
	} printf("\n");				// at the end, new line
}
void printVal(int val[], int size) 		// prints value of an int array
{
	for (int i = 0; i < size; i++) {	// iterates through
		printf("%d", val[i]);		// prints each individual integer
	} printf("\n");				// at the end, new line
}
#endif
