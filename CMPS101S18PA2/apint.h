#ifndef APINT_H
#define APINT_H
typedef struct apint *apintHandle;

// access procedures
void 	Print(apintHandle value);
int	First(apintHandle value);
void	ToString(apintHandle value, char **str);

// manipulation procedures
void	MatchFill(apintHandle *value1, apintHandle value2);
void 	Add(apintHandle *value1, apintHandle value2);
void 	Subtract(apintHandle *value1, apintHandle value2);
void 	Multiply(apintHandle *value1, apintHandle value2);

// creation procedures
void 	Constructor(apintHandle *value);
void 	String_constructor(apintHandle *value, char string[]);
void	Int_constructor(apintHandle *value, int integer);

#endif
