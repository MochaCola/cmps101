#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "helpfulMethods.h"
#include "apint.h"

typedef struct apint {	
        int *m_value;
        int size;
} apint;

// access procedures
void 	Print(apintHandle value) 			// print form: "m_value = #\n"
{
	printf("m_value = ");				// print preliminary statement
	for (int i = 0; i < value->size; i++) {		// iterate through m_value array elements
		printf("%d", value->m_value[i]);	// print each one
	} printf("\n");					// print newline escape sequence
}
int	First(apintHandle value)			// first element function
{
	return value->m_value[0];			// returns first element in m_value
}
void	ToString(apintHandle value, char **str)		// string conversion function
{
	int bump = 0;
	if (First(value) < 0) {
		(*str) = calloc(value->size + 1, sizeof(char));
		bump = 1;
	} else {
		(*str) = calloc(value->size, sizeof(char));	// allocate size of string
	} for (int i = 0; i < value->size; i++) {		// iterate through each element
		if (i == 0 && First(value) < 0) {
			(*str)[i] = '-';
			(*str)[i+bump] = (-1 * value->m_value[i]) + '0';		// insert character conversion of int into string
		} else {
			(*str)[i+bump] = value->m_value[i] + '0';
		}
	}
}

// manipulation procedures
void	Trim(apintHandle *value)			// trims zeros from the front of m_value
{
	printf("trim reached");
	int size = (*value)->size;
	int counter = 0;
	int numZeros = 0;
	char *result;
	while ((*value)->m_value[counter] == 0) {
		numZeros++;
		counter++;
	} if (numZeros == size - 1) {
		Constructor(&(*value));
		return;
	} else {
		result = calloc(size - numZeros, sizeof(char));
		for (int i = numZeros; i < size; i++) {
			result[i - numZeros] = (*value)->m_value[i] + '0';
		} String_constructor(&(*value), result);
	}
}
void	MatchFill(apintHandle *value1, apintHandle value2)	// fills m_value with zeros to match length
{								// of other m_value
	int fill = value2->size - (*value1)->size;
	int filler[fill];
	memset(filler, 0, sizeof(filler));
	int newArray[value2->size];
	for (int i = 0; i < value2->size; i++) {
		if (i < fill) { newArray[i] = filler[i]; }
		else if (i >= fill) { newArray[i] = (*value1)->m_value[i - fill]; }
	} (*value1)->m_value = realloc((*value1)->m_value, value2->size * sizeof(int));
	(*value1)->size = value2->size;
	memcpy((*value1)->m_value, newArray, sizeof(newArray));
}
void 	Add(apintHandle *value1, apintHandle value2)		// addition function
{
	int size1 = (*value1)->size;
	int size2 = value2->size;
	int largestSize;
	if (First((*value1)) < 0 && First(value2) >= 0) {
		char *temp;
		ToString((*value1), &temp);
		substring(&temp, 1, strlen(temp));
		String_constructor(&(*value1), temp);
		Subtract(&value2, (*value1));
		ToString(value2, &temp);
		String_constructor(&(*value1), temp);
		return;
	} if (First((*value1)) >= 0 && First(value2) < 0) {
		char *temp;
                ToString(value2, &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&value2, temp);
                Subtract(&(*value1), value2);
                return;
	} if (size1 >= size2) { 
		largestSize = size1;
		if (size1 != size2) {
			MatchFill(&value2, (*value1));
		}
	} if (size1 < size2) { 
		largestSize = size2;
		MatchFill(&(*value1), value2);
	} int temp[largestSize];
	int rem = 0;
	for (int i = largestSize-1; i >= 0; i--) {
		int v1 = (*value1)->m_value[i];
		int v2 = value2->m_value[i];
		int val = rem + v1 + v2;
		if (val >= 10) {
			rem = 1;
			val -= 10;
		} else { rem = 0; }
		temp[i] = val;
	}
	(*value1)->m_value = realloc((*value1)->m_value, largestSize * sizeof(int));
	(*value1)->size = largestSize;
        memcpy((*value1)->m_value, temp, sizeof(temp));
	if (First((*value1)) == 0) {
		Trim(&(*value1));
	}
}
void 	Subtract(apintHandle *value1, apintHandle value2)	// subtraction function
{
	int size1 = (*value1)->size;
        int size2 = value2->size;
        int largestSize;
	if (First((*value1)) < 0 && First(value2) >= 0) {
                char *temp;
                ToString((*value1), &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&(*value1), temp);
                Add(&value2, (*value1));
                ToString(value2, &temp);
                String_constructor(&(*value1), temp);
		(*value1)->m_value[0] = First((*value1)) * -1;
                return;
        } if (First((*value1)) >= 0 && First(value2) < 0) {
                char *temp;
                ToString(value2, &temp);
                substring(&temp, 1, strlen(temp));
                String_constructor(&value2, temp);
                Add(&(*value1), value2);
                return;
	} if (size1 >= size2) {
		if (memcmp((*value1), value2, size1) == 0) {
			Constructor(&(*value1));
			return;
		} largestSize = size1;
                if (size1 != size2) {
                        MatchFill(&value2, (*value1));
                }
        } if (size1 < size2) {
		if (memcmp(value2, (*value1), size1) > 0) {
			char *temp;
			ToString(value2, &temp);
			apintHandle apintTemp;
			String_constructor(&apintTemp, temp);
			Subtract(&apintTemp, (*value1));
			ToString(apintTemp, &temp);
			String_constructor(&(*value1), temp);
			(*value1)->m_value[0] = First((*value1)) * -1;
			return;
		}
                largestSize = size2;
                MatchFill(&(*value1), value2);
        }
        int temp[largestSize];
        int rem = 0;
        for (int i = largestSize-1; i >= 0; i--) {
                int v1 = (*value1)->m_value[i];
                int v2 = value2->m_value[i];
                int val = rem + (v1 - v2);
                if (val < 0) {
                        rem = -1;
                        val += 10;
                } else { rem = 0; }
                temp[i] = val;
        }
        (*value1)->m_value = realloc((*value1)->m_value, largestSize * sizeof(int));
        (*value1)->size = largestSize;
        memcpy((*value1)->m_value, temp, sizeof(temp));
	if (First((*value1)) == 0) {
                Trim(&(*value1));
        }
}
void 	Multiply(apintHandle *value1, apintHandle value2)	// multiplication function
{
	char *str1;
	char *str2;
	char *result;
	ToString((*value1), &str1);
	ToString(value2, &str2);
	int size = strlen(str2);
	if (size <= 1) {
		if (size == 0) {
			Constructor(&(*value1));
			return;
		} else {
			int num = str2[0] - '0';
			int val = 0;
			int rem = 0;
			char temp[1];
			for (int i = strlen(str1) - 1; i >= 0; i--) {
				val = rem + (str1[i] - '0') * num;
				rem = 0;
				if (val >= 10 && i != 0) {
					while (val >= 10) {
						val -= 10;
						rem += 1;
					}
				} temp[0] = val + '0';
				result = calloc(strlen(temp), sizeof(char));
				strcat(temp, result);
				strcpy(result, temp);
			} String_constructor(&(*value1), result);
		} return;
	} else {
		apintHandle product;
		Constructor(&product);
		for (int i = 0; i < size; i++) {
			int num = str2[i] - '0';
			char* temp;
			apintHandle apintNum;
			apintHandle apintRes;
			Int_constructor(&apintNum, num);
			Multiply(&(*value1), apintNum);
			ToString((*value1), &result);
			for (int j = size - i - 1; j > 0; j--) {
				strcat(result, "0");
			} String_constructor(&apintRes, result);
			Add(&product, apintRes);
		}
	}
}


// creation procedures
void 	Constructor(apintHandle *value)
{
	(*value) = malloc(sizeof(apint));		// allocate apint value to the size of an apint (16 bytes)
        (*value)->m_value = calloc(1, sizeof(int));     // allocate m_value c-string to one int element
        (*value)->m_value[0] = 0;                       // set that one int element equal to 0
        (*value)->size = 1;                             // set size to one to represent one element
}
void 	String_constructor(apintHandle *value, char str[])
{
	int strLen = strlen(str);				// size variable to be used throughout method
	int polarity = 1;					// variable to represent positivity or negativity
	(*value) = malloc(sizeof(apint));			// allocate apint value to the size of an apint (16 bytes)
	if (str[0] == '-' || str[0] == '+') {			// if str char is '-' or '+'...
		if (str[0] == '-') { polarity = -1; }		// if '-', set polarity to -1
		substring(&str, 1, strLen);			// remove the '-' or '+' from str[] using substring method
		strLen--;					// decrement strLen by 1 due to this change
	}
	(*value)->m_value = calloc(strLen, sizeof(int));	// allocate strLen int elements
	(*value)->size = strLen;				// set size to strLen to represent strLen elements
	for (int i = 0; i < strLen; i++) {			// iterate through each string character
		int strVal = str[i] - '0';			// variable to represent integer conversion of string character
		(*value)->m_value[i] = polarity * strVal;	// insert value of polarity * strVal into m_value
		if (polarity == -1) { polarity = 1; }		// polarity of -1 should only affect the first character of m_value
	}
}
void 	Int_constructor(apintHandle *value, int integer)
{
	char str[10];						// declare string of length at most 10 (because range of int is 10)
	sprintf(str, "%d", integer);				// convert integer to string with sprintf()
	String_constructor(&(*value), str);			// pass string and apint to string constructor
}
