#include <stdio.h>
#include <stdlib.h>
#include "apint.h"

void reconstruct1(apintHandle *test1, apintHandle *test2); 
void reconstruct2(apintHandle *test3, apintHandle *test4);
void reconstruct3(apintHandle *test5, apintHandle *test6);

int main(void) {
	apintHandle test1;
        apintHandle test2;
        apintHandle test3;
        apintHandle test4;
	apintHandle test5;
	apintHandle test6;
	
	// Constructor Tests
	printf("Constructor Tests:\n-----------------------\n");
	printf("Default constructor: (correct result: 0)\n");
	Constructor(&test1);
	Print(test1);
	printf("Integer constructor: (correct result: 1223334444)\n");
	Int_constructor(&test2, 1223334444);
	Print(test2);
	printf("String constructor, no sign: (correct result: 12345678901234567890)\n");
	String_constructor(&test3, "12345678901234567890");
	Print(test3);
	printf("String constructor, with '-' : (correct result: -12345678901234567890)\n");
	String_constructor(&test4, "-12345678901234567890");
	Print(test4);
	printf("String constructor: (correct result: -6666664444)\n");
	String_constructor(&test5, "-6666664444");
	Print(test5);
	printf("Integer constructor: (correct result: -1337)\n");
	Int_constructor(&test6, -1337);
	Print(test6);

	// Addition Tests
	printf("\n\nAddition Tests:\n-----------------------\n");
	printf("Adding + to +, same lengths: (correct result: 24691357802469135780)\n");
	Add(&test3, test3);
	Print(test3);
	printf("Adding + to +, len(val1) > len(val2): (correct result: 24691357803692470224)\n");
	Add(&test3, test2);
	Print(test3);
	printf("Adding + to +, len(val1) < len(val2): (correct result: 24691357804915804668)\n");
	Add(&test2, test3);
	Print(test2);
	printf("Adding - to +, same lengths: (correct result: 12345678902457902334)\n");
	Add(&test4, test3);
	Print(test4);
	printf("Adding + to -, len(val1) > len(val2): (correct result: 24691357798249140224)\n");
	Add(&test2, test5);
	Print(test2);
	printf("Adding - to -, len(val1) < len(val2): (correct result: -6666663781)\n");
	Add(&test6, test5);
	Print(test6);
	
	reconstruct1(&test1, &test2);
	reconstruct2(&test3, &test4);
	reconstruct3(&test5, &test6);
	
	// Subtraction Tests
	printf("\n\nSubtraction Tests:\n-----------------------\n");
	printf("Subtracting + from +, same lengths: (correct result: 0)\n");
        Subtract(&test3, test3);
        Print(test3);
        printf("Subtracting + from 0, len(val1) < len(val2): (correct result: -1223334444)\n");
        Subtract(&test3, test2);
        Print(test3);
        printf("Subtracting - from +, same lengths: (correct result: 2446668888)\n");
        Subtract(&test2, test3);
        Print(test2);
        printf("Subtracting + from -, len(val1) > len(val2): (correct result: -12345678903681236778)\n");
        Subtract(&test4, test2);
        Print(test4);
        printf("Subtracting - from +, len(val1) > len(val2): (correct result: 12345678910347901222)\n");
        Subtract(&test2, test5);
        Print(test2);
	printf("Subtracting - from -, same lengths: (correct result: 5333334893)\n");
	Subtract(&test6, test5);
        Print(test6);

	reconstruct1(&test1, &test2);
        reconstruct2(&test3, &test4);
        reconstruct3(&test5, &test6);

	// Multiplication Tests
	printf("\n\nDespite my best efforts... I couldn't get multiplication to work as I intended.\n");
	printf("Multiplication Tests:\n-----------------------\n");
	printf("Multiplying + by +, val1 > val2: (correct result: 15102894234444321023293403160)\n");
	Multiply(&test2, test3);
	Print(test2);
	return 0;
}

void reconstruct1(apintHandle *test1, apintHandle *test2)
{
	Constructor(&(*test1));
        Int_constructor(&(*test2), 1223334444);
}	
void reconstruct2(apintHandle *test3, apintHandle *test4)
{
	String_constructor(&(*test3), "12345678901234567890");
        String_constructor(&(*test4), "-12345678901234567890");
}
void reconstruct3(apintHandle *test5, apintHandle *test6)
{
        String_constructor(&(*test5), "-6666664444");
	Int_constructor(&(*test6), -1337);
}
