import sourcefiles.apint;

public class aprat {
	apint m_numerator;
	apint m_denominator;

	// default constructor
	public aprat() {
		m_numerator = new apint();
		m_denominator = new apint(1);
	}

	// parameterized constructor with two apint parameters
	public aprat(apint num, apint den) {
		m_numerator = num;
		m_denominator = den;
	}

	// parameterized constructor with two int parameters
	public aprat(int num, int den) {
		m_numerator = new apint(num);
		m_denominator = new apint(den);
	}

	// parameterized constructor with real value and precision (precision 1 = 1 decimal place)
	public aprat(double val, int precision) {
		String stringVal = Double.toString(val);
		int int_digits = stringVal.indexOf('.');
		int dec_digits = stringVal.length() - int_digits - 1;
		if (precision > dec_digits) {
			while (precision != dec_digits) {
				stringVal += "0";
				dec_digits++;
			}
		} m_numerator = new apint(stringVal.substring(0, int_digits) + stringVal.substring(int_digits + 1));
		String temp = "1";
		for (int i = 0; i < dec_digits; i++) {
			temp += "0";
		} m_denominator = new apint(temp);
	}

	// print function
	void print() {
		System.out.println(m_numerator + "/" + m_denominator);
	}

	// addition function
	aprat add(aprat value) {
		apint this_numerator = this.m_numerator.multiply(value.m_denominator);
		apint value_numerator = value.m_numerator.multiply(this.m_denominator);
		apint common_denominator = this.m_denominator.multiply(value.m_denominator);
		return new aprat(this_numerator.add(value_numerator), common_denominator);
	}

	// subtraction function
	aprat subtract(aprat value) {
		apint this_numerator = this.m_numerator.multiply(value.m_denominator);
                apint value_numerator = value.m_numerator.multiply(this.m_denominator);
                apint common_denominator = this.m_denominator.multiply(value.m_denominator);
                return new aprat(this_numerator.subtract(value_numerator), common_denominator);
	}

	// multiplication function
	aprat multiply(aprat value) {
		apint numerator = this.m_numerator.multiply(value.m_numerator);
                apint denominator = this.m_denominator.multiply(value.m_denominator);
                return new aprat(numerator, denominator);
	}

	// division function
	aprat divide(aprat value) {
		apint numerator = this.m_numerator.multiply(value.m_denominator);
		apint denominator = this.m_denominator.multiply(value.m_numerator);
		return new aprat(numerator, denominator);
	}

	public static void main(String args[]) {
		aprat test1 = new aprat(69.777888999, 10);
		aprat test2 = new aprat(1732, 90210);
		aprat test3 = new aprat(1124, 4367);
		aprat test4 = test2.add(test3);
		aprat test5 = test2.subtract(test3);
		aprat test6 = test2.multiply(test3);
		aprat test7 = test2.divide(test3);
		test1.print();
		test4.print();
		test5.print();
		test6.print();
		test7.print();
	}
}
