Script started on 2019-01-30 12:01:36-0800
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ pwd
/home/kenny/CMPS101/CMPS101S18PA1
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ ls -l
total 56
-rw-r--r-- 1 kenny kenny  9238 Jan 30 12:00 apint.class
-rw-r--r-- 1 kenny kenny 16823 Jan 30 11:56 apint.java
-rw-r--r-- 1 kenny kenny  2691 Jan 30 11:50 aprat.class
-rw-r--r-- 1 kenny kenny  2887 Jan 30 11:30 aprat.java
-rw-r--r-- 1 kenny kenny  2562 Jan 30 12:01 ExtraCredit.txt
-rw-r--r-- 1 kenny kenny  1973 Jan 30 11:51 NoteToGrader
-rw-r--r-- 1 kenny kenny     0 Jan 30 12:01 pa1submissionfile.txt
-rw-r--r-- 1 kenny kenny   260 Jan 30 11:31 README
drwxr-xr-x 2 kenny kenny  4096 Jan 30 12:00 [0m[01;34msourcefiles[0m
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ cat README
apint.java - Implementation and test file for apint class
aprat.java - Implementation and test file for aprat class
apint.class - Binary file for apint class
aprat.class - Binary file for aprat class
ExtraCredit.txt - Output of 1000 factorial for extra credit
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ cat NoteToGrader
Hi there, I hope you find my code satisfactory. I apologize for the late submission, but I was faced with a time constraint
and decided it would be better to submit my code late & working as opposed to on time and not performing correctly. The end
result has achieved what I hoped to accomplish!

I implemented my apint class variables with an array of Longs. I chose to use Longs because I found that they have the
highest digit/byte ratio, thus making it an optimal choice for running time if we are not allowed to use BigIntegers.
Each long value in the array has a maximum length of 19 digits, which is sometimes cut to 18 if the Long value attempting
to be stored exceeds Long.MAX_VALUE. On occasion, individual 0s are stored in the array to avoid loss of digits due to
left integer padding (the compiler will remove left-most zeros in a Long value such as 00000000005).

I opted to run my test cases in my .java files. I have ensured that the solutions you see in my outputs are correct.
I hope you will find my test cases adequate.

You will find that my apint class is capable of:
- Performing operations (+, -, *, /) on both positive and negative numbers. The user can specify two negative numbers
  if they choose. The apint class can also handle factorials, such as the extra credit 1000 factorial.
- apint variables can be created with: no argument, integer arguments, double arguments, and string arguments of arbitrary 
  length with an optional + or - sign at the front.
- Being displayed, as well as being converted to a numeric string for various purposes.
- Filling arrays with zeros to match the length of another array.

You will find that my aprat class is cabable of:
- Performing operations (+, -, *, /) on both positive and negative numbers. The user can specify two negative numbers
  if they choose.
- aprat variables can be created with: no argument, two integer arguments, and a double argument and specified index precision.
- Being displayed.
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ cat apint.java
import java.util.ArrayList;
import java.lang.Math;
import java.io.File;
import java.io.PrintWriter;

public class apint {
	private ArrayList<Long> m_value = new ArrayList<Long>();
	
	// default constructor
	public apint() {
		m_value.add((long) 0);
	}

	// parameterized constructor with string parameter
	public apint(String p_value) {
		String str = p_value;
                char first = str.charAt(0);
		int polarity = 1;
		int counter = 0;
		long val;
		long max = Long.parseLong((Long.toString(Long.MAX_VALUE)).substring(0, 18));
                if (first == '-' || first == '+') {
			str = str.substring(1);
			if (first == '-') {
				polarity = -1;
			}
		} int size = str.length();
		int elements = (int) (Math.ceil(size / 19.0));
		for (int i = 0; i < elements; i++) {
			int index1 = 19 * i;
			int index2 = 19 * (i + 1);
			if (index1 + counter < size) {
				if (i != (elements - 1) && (index2 + counter) < size) {
					String temp = str.substring(index1 + counter, index2 + counter);
                        		if (Long.parseLong(temp.substring(0, 18)) >= max) {
						val = Long.parseLong(temp.substring(0, 18));
						counter--;
					} else {
						temp = str.substring(index1 + counter, index2 + counter);
						val = Long.parseLong(temp);
					}
                                } else {
					String temp = "";
					if (index1 + counter + 19 < size) {
						temp = str.substring(index1 + counter, index2 + counter);
					} else {
						temp = str.substring(index1 + counter);
					}
					val = Long.parseLong(temp);
                                } if (i == 0) {
                                        val *= polarity;
                                } m_value.add(val);
                                while ((index2 + counter < size) && str.charAt(index2 + counter) == '0') {
                                        m_value.add((long) 0);
                                        counter++;
                                }
			} else {
				break;
			}
		}
        }

	// parameterized constructor with int parameter
	public apint(int p_value) {
		m_value.add((long) p_value);
	}

	// parameterized constructor with real parameter
	public apint(double p_value) {
		m_value.add((long) p_value);
	}

	// function that returns string equivalent of apint variable
	@Override
        public String toString() {
                String result = "";
                for (int i = 0; i < m_value.size(); i++) {
                        result += String.valueOf(m_value.get(i));
                } return result;
        }

	// print function
	public void print() {
		System.out.println("m_value: " + this.toString());
	}

	// function that returns the first character/digit of apint variable
	public char first() {
		return (String.valueOf(m_value.get(0))).charAt(0);
	}

	// size function (will return number of digits in apint variable, excluding '-')
	public int size() {
		String val = this.toString();
		if (val.charAt(0) == '-') {
			val = val.substring(1);
		} return val.length();
	}

	// function that fills a string equivalent to apint variable with zeros until it matches another apint variable in size
	private String matchFill(apint value, String string) {
		int fill = this.size() - value.size();
		String filler, result;
	        filler = result = "";
                for (int i = 0; i < fill; i++) {
                	filler += "0";
                } 
		result = filler + string;
		return result;
	}

	// addition function
	public apint add(apint value) {
		String string1 = this.toString();
		String string2 = value.toString();
		String result = "";
		int size = 0;
		int rem = 0;
		if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
			apint dif = temp.subtract(value);
			result = "-" + dif.toString();
			return new apint(result);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((value.toString()).substring(1));
			apint dif = this.subtract(temp);
			result = dif.toString();
			return new apint(result);
                } if (this.first() == '-' && value.first() == '-') {
			apint temp1 = new apint((this.toString()).substring(1));
			apint temp2 = new apint((value.toString()).substring(1));
			apint sum = temp1.add(temp2);
			result = "-" + sum.toString();
			return new apint(result);
		} if (this.size() > value.size()) {
			string2 = this.matchFill(value, string2);
			size = string2.length();
		} if (value.size() > this.size()) {
                        string1 = value.matchFill(this, string1);
			size = string1.length();
                } if (value.size() == this.size()) {
			size = this.size();
		} for (int i = (size - 1); i >= 0; i--) {
			int sum = rem + Character.getNumericValue(string1.charAt(i)) + Character.getNumericValue(string2.charAt(i));
			rem = 0;
			if (sum >= 10 && i != 0) {
				sum -= 10;
				rem = 1;
			} result = Integer.toString(sum) + result;
		} return new apint(result);
	}

	// subtraction function
	public apint subtract(apint value) {
		String string1 = this.toString();
                String string2 = value.toString();
                String result = "";
		int size = 0;
		int rem = 0;
		if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
                        apint sum = temp.add(value);
                        result = "-" + sum.toString();
                        return new apint(result);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((value.toString()).substring(1));
                        apint sum = this.add(temp);
                        result = sum.toString();
                        return new apint(result);
		} if (this.first() == '-' && value.first() == '-') {
                        apint temp1 = new apint((this.toString()).substring(1));
                        apint temp2 = new apint((value.toString()).substring(1));
                        apint dif = temp1.subtract(temp2);
                        result = "-" + dif.toString();
                        return new apint(result);
		} if (string1.length() <= 19 && string2.length() <= 19) {
                        if (Long.parseLong(string1) < Long.parseLong(string2)) {
                                apint temp = value.subtract(this);
                                return new apint("-" + temp.toString());
                        }
                } if (this.size() > value.size()) {
                        string2 = this.matchFill(value, string2);
                        size = string2.length();
                } if (value.size() > this.size()) {
                        string1 = value.matchFill(this, string1);
                        size = string1.length();
                } if (value.size() == this.size()) {
                        size = this.size();
                } for (int i = (size - 1); i >= 0; i--) {
			int dif = 0;
			if (this.size() >= value.size()) {
				dif = rem + Character.getNumericValue(string1.charAt(i)) - Character.getNumericValue(string2.charAt(i));
			} if (value.size() > this.size()) {
				dif = rem + Character.getNumericValue(string2.charAt(i)) - Character.getNumericValue(string1.charAt(i));
			} rem = 0;
                        if (dif < 0 && i != 0) {
                                dif += 10;
                                rem = -1;
                        } result = Integer.toString(dif) + result;
                } if (value.size() > this.size()) {
			result = "-" + result;
		} return new apint(result);
	}

	// multiplication function
	public apint multiply(apint value) {
		String string1 = this.toString();
                String string2 = value.toString();
                int size = string2.length();
                String result = "";
		apint product = new apint();
                if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
			result = (temp.multiply(value)).toString();
			return new apint("-" + result);
		} if (this.first() != '-' && value.first() == '-') {
			apint temp = new apint((value.toString()).substring(1));
			result = (this.multiply(temp)).toString();
			return new apint("-" + result);
		} if (this.first() == '-' && value.first() == '-') {
			apint temp1 = new apint((this.toString()).substring(1));
			apint temp2 = new apint((value.toString()).substring(1));
			return temp1.multiply(temp2);
		} if (size <= 1) {
			if (size == 0) {
				return new apint();
			} else {
				int num = Character.getNumericValue(string2.charAt(0));
				int prod = 0;
				int rem = 0;
				for (int i = string1.length() - 1; i >= 0; i--) {
					prod = rem + (Character.getNumericValue(string1.charAt(i)) * num);
					rem = 0;
					if (prod >= 10 && i != 0) {
						while (prod >= 10) {
							prod -= 10;
							rem += 1;
						}
					} result = Integer.toString(prod) + result;
				} return new apint(result);
			}
		} else {
			for (int i = 0; i < size; i++) {
                        	int num = Character.getNumericValue(string2.charAt(i));
                        	result = (this.multiply(new apint(num))).toString();
                        	for (int j = size - i - 1; j > 0; j--) {
                                	result += "0";
                        	}
				product = product.add(new apint(result));
                	} return product;
		}
	}

	// division function
        public apint divide(apint value) {
		String str_dividend = this.toString();
                String str_divisor = value.toString();
                String quotient = "";
		if (this.first() == '-' && value.first() != '-') {
                        apint temp = new apint((str_dividend).substring(1));
                        quotient = (temp.divide(value)).toString();
                        return new apint("-" + quotient);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((str_divisor).substring(1));
                        quotient = (this.divide(temp)).toString();
                        return new apint("-" + quotient);
                } if (this.first() == '-' && value.first() == '-') {
                        apint temp1 = new apint((str_dividend).substring(1));
                        apint temp2 = new apint((str_divisor).substring(1));
                        return temp1.divide(temp2);
		} int quotientSize = str_dividend.length() - str_divisor.length();
		if (quotientSize < 0 || str_divisor == "0") {
			return new apint();
		} else if (quotientSize == 0) {
			int endBound;
			if (str_dividend.length() >= 19) {
				endBound = 19;
			} else {
				endBound = str_dividend.length();
			} String temp1 = str_dividend.substring(0, endBound);
			if (str_divisor.length() >= 19) {
                                endBound = 19;
                        } else {
                                endBound = str_divisor.length();
                        } String temp2 = str_divisor.substring(0, endBound); 
                        if (Long.parseLong(temp1) < Long.parseLong(temp2)) {
				return new apint();
			} else if (Long.parseLong(temp1) >= Long.parseLong(temp2)) {
				return new apint(1);
			}
		} int index = str_divisor.length() + 1;
                apint dividend = new apint(str_dividend.substring(0, index));
                apint divisor = new apint(str_divisor);
                for (int i = 0; i < quotientSize; i++) {
			int counter = 0;
                        while ((dividend.subtract(divisor.multiply(new apint(counter)))).first() != '-') {
				counter++;
			} counter -= 1;
			quotient += (Integer.toString(counter));
			dividend = dividend.subtract(divisor.multiply(new apint(counter)));
			String temp = dividend.toString();
			if (index <= (str_dividend.length() - 1)) {
				temp += str_dividend.charAt(index);
				dividend = new apint(temp);
				index++;
			}
                } return new apint(quotient);
        }

	public static void main(String args[]) {
		// Constructor Tests
		System.out.println("Constructor Tests:");
		apint test1 = new apint();
		apint test2 = new apint("-11112222333344445555666677778888999900000000999988887777666655554444333322221111");
		apint test3 = new apint(1000000000);
		apint test4 = new apint(1000000000.6574839201);
		apint test5 = new apint("+12345678901234567890");
		apint test6 = new apint("+55555666666");
		apint test7 = new apint("-12345678901234567890");
		apint test8 = new apint("-55555666666");
		apint test9 = new apint("+999999999999999999");
		apint test10 = new apint("-999999999999999999");
		test1.print();
		test2.print();
		test3.print();
		test4.print();
		test5.print();
		test6.print();
		test7.print();
		test8.print();
		test9.print();
		test10.print();

		// Addition Tests
		System.out.println("\nAddition Tests:");
		apint addTest1 = test5.add(test6);
		apint addTest2 = test6.add(test5);
		apint addTest3 = test7.add(test6);
		apint addTest4 = test6.add(test7);
		apint addTest5 = test5.add(test8);
		apint addTest6 = test8.add(test5);
		apint addTest7 = test7.add(test8);
		apint addTest8 = test8.add(test7);
		apint addTest9 = test9.add(test9);
		apint addTest10 = test9.add(test10);
		apint addTest11 = test10.add(test9);
		apint addTest12 = test10.add(test10);
		addTest1.print();
		addTest2.print();
		addTest3.print();
		addTest4.print();
		addTest5.print();
		addTest6.print();
		addTest7.print();
		addTest8.print();
		addTest9.print();
		addTest10.print();
		addTest11.print();
		addTest12.print();

		// Subtraction Tests
		System.out.println("\nSubtraction Tests:");
		apint subTest1 = test5.subtract(test6);
		apint subTest2 = test6.subtract(test5);
		apint subTest3 = test7.subtract(test6);
		apint subTest4 = test6.subtract(test7);
		apint subTest5 = test5.subtract(test8);
		apint subTest6 = test8.subtract(test5);
		apint subTest7 = test7.subtract(test8);
		apint subTest8 = test8.subtract(test7);
		apint subTest9 = test9.subtract(test9);
		apint subTest10 = test9.subtract(test10);
		apint subTest11 = test10.subtract(test9);
		apint subTest12 = test10.subtract(test10);
		subTest1.print();
		subTest2.print();
		subTest3.print();
		subTest4.print();
		subTest5.print();
		subTest6.print();
		subTest7.print();
		subTest8.print();
		subTest9.print();
		subTest10.print();
		subTest11.print();
		subTest12.print();

		//  Multiplication Tests
		System.out.println("\nMultiplication Tests:");
		apint multTest1 = test5.multiply(test6);
		apint multTest2 = test6.multiply(test5);
		apint multTest3 = test7.multiply(test6);
		apint multTest4 = test6.multiply(test7);
		apint multTest5 = test5.multiply(test8);
		apint multTest6 = test8.multiply(test5);
		apint multTest7 = test7.multiply(test8);
		apint multTest8 = test8.multiply(test7);
		apint multTest9 = test9.multiply(test9);
		apint multTest10 = test9.multiply(test10);
		apint multTest11 = test10.multiply(test9);
		apint multTest12 = test10.multiply(test10);
		multTest1.print();
		multTest2.print();
		multTest3.print();
		multTest4.print();
		multTest5.print();
		multTest6.print();
		multTest7.print();
		multTest8.print();
		multTest9.print();
		multTest10.print();
		multTest11.print();
		multTest12.print();

		// Division Tests
		System.out.println("\nDivision Tests:");
		apint divTest1 = test5.divide(test6);
		apint divTest2 = test6.divide(test5);
		apint divTest3 = test7.divide(test6);
		apint divTest4 = test6.divide(test7);
		apint divTest5 = test5.divide(test8);
		apint divTest6 = test8.divide(test5);
		apint divTest7 = test7.divide(test8);
		apint divTest8 = test8.divide(test7);
		apint divTest9 = test9.divide(test9);
		apint divTest10 = test9.divide(test10);
		apint divTest11 = test10.divide(test9);
		apint divTest12 = test10.divide(test10);
		divTest1.print();
		divTest2.print();
		divTest3.print();
		divTest4.print();
		divTest5.print();
		divTest6.print();
		divTest7.print();
		divTest8.print();
		divTest9.print();
		divTest10.print();
		divTest11.print();
		divTest12.print();

		// Extra Credit: 1000!
		System.out.println("\nExtra Credit: 1000!");
		apint factorial = new apint(1);
		for (int i = 1; i <= 1000; i++) {
			factorial = factorial.multiply(new apint(i));
		} String hugeFactorial = factorial.toString();
		int index = (int) Math.ceil(hugeFactorial.length() / 60);
		File file = new File("/home/kenny/CMPS101/CMPS101S18PA1/ExtraCredit.txt");
		PrintWriter out = null;
		try {
		out = new PrintWriter(file);
		for (int i = 0; i < index; i++) {
				if (i != index) {
					out.println(hugeFactorial.substring(60 * i, 60 * (i + 1)));
				} else {
					out.println(hugeFactorial.substring(60 * i));
				}
			}
		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
				System.out.println("The value of 1000! output to ExtraCredit.txt!");
			}
		}
	}
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ cat aprar[Kt.java
import sourcefiles.apint;

public class aprat {
	apint m_numerator;
	apint m_denominator;

	// default constructor
	public aprat() {
		m_numerator = new apint();
		m_denominator = new apint(1);
	}

	// parameterized constructor with two apint parameters
	public aprat(apint num, apint den) {
		m_numerator = num;
		m_denominator = den;
	}

	// parameterized constructor with two int parameters
	public aprat(int num, int den) {
		m_numerator = new apint(num);
		m_denominator = new apint(den);
	}

	// parameterized constructor with real value and precision (precision 1 = 1 decimal place)
	public aprat(double val, int precision) {
		String stringVal = Double.toString(val);
		int int_digits = stringVal.indexOf('.');
		int dec_digits = stringVal.length() - int_digits - 1;
		if (precision > dec_digits) {
			while (precision != dec_digits) {
				stringVal += "0";
				dec_digits++;
			}
		} m_numerator = new apint(stringVal.substring(0, int_digits) + stringVal.substring(int_digits + 1));
		String temp = "1";
		for (int i = 0; i < dec_digits; i++) {
			temp += "0";
		} m_denominator = new apint(temp);
	}

	// print function
	void print() {
		System.out.println(m_numerator + "/" + m_denominator);
	}

	// addition function
	aprat add(aprat value) {
		apint this_numerator = this.m_numerator.multiply(value.m_denominator);
		apint value_numerator = value.m_numerator.multiply(this.m_denominator);
		apint common_denominator = this.m_denominator.multiply(value.m_denominator);
		return new aprat(this_numerator.add(value_numerator), common_denominator);
	}

	// subtraction function
	aprat subtract(aprat value) {
		apint this_numerator = this.m_numerator.multiply(value.m_denominator);
                apint value_numerator = value.m_numerator.multiply(this.m_denominator);
                apint common_denominator = this.m_denominator.multiply(value.m_denominator);
                return new aprat(this_numerator.subtract(value_numerator), common_denominator);
	}

	// multiplication function
	aprat multiply(aprat value) {
		apint numerator = this.m_numerator.multiply(value.m_numerator);
                apint denominator = this.m_denominator.multiply(value.m_denominator);
                return new aprat(numerator, denominator);
	}

	// division function
	aprat divide(aprat value) {
		apint numerator = this.m_numerator.multiply(value.m_denominator);
		apint denominator = this.m_denominator.multiply(value.m_numerator);
		return new aprat(numerator, denominator);
	}

	public static void main(String args[]) {
		aprat test1 = new aprat(69.777888999, 10);
		aprat test2 = new aprat(1732, 90210);
		aprat test3 = new aprat(1124, 4367);
		aprat test4 = test2.add(test3);
		aprat test5 = test2.subtract(test3);
		aprat test6 = test2.multiply(test3);
		aprat test7 = test2.divide(test3);
		test1.print();
		test4.print();
		test5.print();
		test6.print();
		test7.print();
	}
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ java apint
Constructor Tests:
m_value: 0
m_value: -11112222333344445555666677778888999900000000999988887777666655554444333322221111
m_value: 1000000000
m_value: 1000000000
m_value: 12345678901234567890
m_value: 55555666666
m_value: -12345678901234567890
m_value: -55555666666
m_value: 999999999999999999
m_value: -999999999999999999

Addition Tests:
m_value: 12345678956790234556
m_value: 12345678956790234556
m_value: -12345678845678901224
m_value: -12345678845678901224
m_value: 12345678845678901224
m_value: 12345678845678901224
m_value: -12345678956790234556
m_value: -12345678956790234556
m_value: 1999999999999999998
m_value: 0
m_value: 0
m_value: -1999999999999999998

Subtraction Tests:
m_value: 12345678845678901224
m_value: -12345678845678901224
m_value: -12345678956790234556
m_value: 12345678956790234556
m_value: 12345678956790234556
m_value: -12345678956790234556
m_value: -12345678845678901224
m_value: 12345678845678901224
m_value: 0
m_value: 1999999999999999998
m_value: -1999999999999999998
m_value: 0

Multiplication Tests:
m_value: 685872421802456789573386954740
m_value: 685872421802456789573386954740
m_value: -685872421802456789573386954740
m_value: -685872421802456789573386954740
m_value: -685872421802456789573386954740
m_value: -685872421802456789573386954740
m_value: 685872421802456789573386954740
m_value: 685872421802456789573386954740
m_value: 999999999999999998000000000000000001
m_value: -999999999999999998000000000000000001
m_value: -999999999999999998000000000000000001
m_value: 999999999999999998000000000000000001

Division Tests:
m_value: 222221775
m_value: 0
m_value: -222221775
m_value: 0
m_value: -222221775
m_value: 0
m_value: 222221775
m_value: 0
m_value: 1
m_value: -1
m_value: -1
m_value: 1

Extra Credit: 1000!
The value of 1000! output to ExtraCredit.txt!
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ cat ExtraCredit.txt
402387260077093773543702433923003985719374864210714632543799
910429938512398629020592044208486969404800479988610197196058
631666872994808558901323829669944590997424504087073759918823
627727188732519779505950995276120874975462497043601418278094
646496291056393887437886487337119181045825783647849977012476
632889835955735432513185323958463075557409114262417474349347
553428646576611667797396668820291207379143853719588249808126
867838374559731746136085379534524221586593201928090878297308
431392844403281231558611036976801357304216168747609675871348
312025478589320767169132448426236131412508780208000261683151
027341827977704784635868170164365024153691398281264810213092
761244896359928705114964975419909342221566832572080821333186
116811553615836546984046708975602900950537616475847728421889
679646244945160765353408198901385442487984959953319101723355
556602139450399736280750137837615307127761926849034352625200
015888535147331611702103968175921510907788019393178114194545
257223865541461062892187960223838971476088506276862967146674
697562911234082439208160153780889893964518263243671616762179
168909779911903754031274622289988005195444414282012187361745
992642956581746628302955570299024324153181617210465832036786
906117260158783520751516284225540265170483304226143974286933
061690897968482590125458327168226458066526769958652682272807
075781391858178889652208164348344825993266043367660176999612
831860788386150279465955131156552036093988180612138558600301
435694527224206344631797460594682573103790084024432438465657
245014402821885252470935190620929023136493273497565513958720
559654228749774011413346962715422845862377387538230483865688
976461927383814900140767310446640259899490222221765904339901
886018566526485061799702356193897017860040811889729918311021
171229845901641921068884387121855646124960798722908519296819
372388642614839657382291123125024186649353143970137428531926
649875337218940694281434118520158014123344828015051399694290
153483077644569099073152433278288269864602789864321139083506
217095002597389863554277196742822248757586765752344220207573
630569498825087968928162753848863396909959826280956121450994
871701244516461260379029309120889086942028510640182154399457
156805941872748998094254742173582401063677404595741785160829
230135358081840096996372524230560855903700624271243416909004
153690105933983835777939410970027753472000000000000000000000
000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000
000000000000000000000000000000000000000000000000000000000000
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ java aprat
697778889990/10000000000
108959684/393947070
-93832396/393947070
1946768/393947070
7563644/101396040
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA1[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA1[00m$ exit
exit

Script done on 2019-01-30 12:03:36-0800
