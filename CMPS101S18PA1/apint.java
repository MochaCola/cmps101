import java.util.ArrayList;
import java.lang.Math;
import java.io.File;
import java.io.PrintWriter;

public class apint {
	private ArrayList<Long> m_value = new ArrayList<Long>();
	
	// default constructor
	public apint() {
		m_value.add((long) 0);
	}

	// parameterized constructor with string parameter
	public apint(String p_value) {
		String str = p_value;
                char first = str.charAt(0);
		int polarity = 1;
		int counter = 0;
		long val;
		long max = Long.parseLong((Long.toString(Long.MAX_VALUE)).substring(0, 18));
                if (first == '-' || first == '+') {
			str = str.substring(1);
			if (first == '-') {
				polarity = -1;
			}
		} int size = str.length();
		int elements = (int) (Math.ceil(size / 19.0));
		for (int i = 0; i < elements; i++) {
			int index1 = 19 * i;
			int index2 = 19 * (i + 1);
			if (index1 + counter < size) {
				if (i != (elements - 1) && (index2 + counter) < size) {
					String temp = str.substring(index1 + counter, index2 + counter);
                        		if (Long.parseLong(temp.substring(0, 18)) >= max) {
						val = Long.parseLong(temp.substring(0, 18));
						counter--;
					} else {
						temp = str.substring(index1 + counter, index2 + counter);
						val = Long.parseLong(temp);
					}
                                } else {
					String temp = "";
					if (index1 + counter + 19 < size) {
						temp = str.substring(index1 + counter, index2 + counter);
					} else {
						temp = str.substring(index1 + counter);
					}
					val = Long.parseLong(temp);
                                } if (i == 0) {
                                        val *= polarity;
                                } m_value.add(val);
                                while ((index2 + counter < size) && str.charAt(index2 + counter) == '0') {
                                        m_value.add((long) 0);
                                        counter++;
                                }
			} else {
				break;
			}
		}
        }

	// parameterized constructor with int parameter
	public apint(int p_value) {
		m_value.add((long) p_value);
	}

	// parameterized constructor with real parameter
	public apint(double p_value) {
		m_value.add((long) p_value);
	}

	// function that returns string equivalent of apint variable
	@Override
        public String toString() {
                String result = "";
                for (int i = 0; i < m_value.size(); i++) {
                        result += String.valueOf(m_value.get(i));
                } return result;
        }

	// print function
	public void print() {
		System.out.println("m_value: " + this.toString());
	}

	// function that returns the first character/digit of apint variable
	public char first() {
		return (String.valueOf(m_value.get(0))).charAt(0);
	}

	// size function (will return number of digits in apint variable, excluding '-')
	public int size() {
		String val = this.toString();
		if (val.charAt(0) == '-') {
			val = val.substring(1);
		} return val.length();
	}

	// function that fills a string equivalent to apint variable with zeros until it matches another apint variable in size
	private String matchFill(apint value, String string) {
		int fill = this.size() - value.size();
		String filler, result;
	        filler = result = "";
                for (int i = 0; i < fill; i++) {
                	filler += "0";
                } 
		result = filler + string;
		return result;
	}

	// addition function
	public apint add(apint value) {
		String string1 = this.toString();
		String string2 = value.toString();
		String result = "";
		int size = 0;
		int rem = 0;
		if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
			apint dif = temp.subtract(value);
			result = "-" + dif.toString();
			return new apint(result);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((value.toString()).substring(1));
			apint dif = this.subtract(temp);
			result = dif.toString();
			return new apint(result);
                } if (this.first() == '-' && value.first() == '-') {
			apint temp1 = new apint((this.toString()).substring(1));
			apint temp2 = new apint((value.toString()).substring(1));
			apint sum = temp1.add(temp2);
			result = "-" + sum.toString();
			return new apint(result);
		} if (this.size() > value.size()) {
			string2 = this.matchFill(value, string2);
			size = string2.length();
		} if (value.size() > this.size()) {
                        string1 = value.matchFill(this, string1);
			size = string1.length();
                } if (value.size() == this.size()) {
			size = this.size();
		} for (int i = (size - 1); i >= 0; i--) {
			int sum = rem + Character.getNumericValue(string1.charAt(i)) + Character.getNumericValue(string2.charAt(i));
			rem = 0;
			if (sum >= 10 && i != 0) {
				sum -= 10;
				rem = 1;
			} result = Integer.toString(sum) + result;
		} return new apint(result);
	}

	// subtraction function
	public apint subtract(apint value) {
		String string1 = this.toString();
                String string2 = value.toString();
                String result = "";
		int size = 0;
		int rem = 0;
		if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
                        apint sum = temp.add(value);
                        result = "-" + sum.toString();
                        return new apint(result);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((value.toString()).substring(1));
                        apint sum = this.add(temp);
                        result = sum.toString();
                        return new apint(result);
		} if (this.first() == '-' && value.first() == '-') {
                        apint temp1 = new apint((this.toString()).substring(1));
                        apint temp2 = new apint((value.toString()).substring(1));
                        apint dif = temp1.subtract(temp2);
                        result = "-" + dif.toString();
                        return new apint(result);
		} if (string1.length() <= 19 && string2.length() <= 19) {
                        if (Long.parseLong(string1) < Long.parseLong(string2)) {
                                apint temp = value.subtract(this);
                                return new apint("-" + temp.toString());
                        }
                } if (this.size() > value.size()) {
                        string2 = this.matchFill(value, string2);
                        size = string2.length();
                } if (value.size() > this.size()) {
                        string1 = value.matchFill(this, string1);
                        size = string1.length();
                } if (value.size() == this.size()) {
                        size = this.size();
                } for (int i = (size - 1); i >= 0; i--) {
			int dif = 0;
			if (this.size() >= value.size()) {
				dif = rem + Character.getNumericValue(string1.charAt(i)) - Character.getNumericValue(string2.charAt(i));
			} if (value.size() > this.size()) {
				dif = rem + Character.getNumericValue(string2.charAt(i)) - Character.getNumericValue(string1.charAt(i));
			} rem = 0;
                        if (dif < 0 && i != 0) {
                                dif += 10;
                                rem = -1;
                        } result = Integer.toString(dif) + result;
                } if (value.size() > this.size()) {
			result = "-" + result;
		} return new apint(result);
	}

	// multiplication function
	public apint multiply(apint value) {
		String string1 = this.toString();
                String string2 = value.toString();
                int size = string2.length();
                String result = "";
		apint product = new apint();
                if (this.first() == '-' && value.first() != '-') {
			apint temp = new apint((this.toString()).substring(1));
			result = (temp.multiply(value)).toString();
			return new apint("-" + result);
		} if (this.first() != '-' && value.first() == '-') {
			apint temp = new apint((value.toString()).substring(1));
			result = (this.multiply(temp)).toString();
			return new apint("-" + result);
		} if (this.first() == '-' && value.first() == '-') {
			apint temp1 = new apint((this.toString()).substring(1));
			apint temp2 = new apint((value.toString()).substring(1));
			return temp1.multiply(temp2);
		} if (size <= 1) {
			if (size == 0) {
				return new apint();
			} else {
				int num = Character.getNumericValue(string2.charAt(0));
				int prod = 0;
				int rem = 0;
				for (int i = string1.length() - 1; i >= 0; i--) {
					prod = rem + (Character.getNumericValue(string1.charAt(i)) * num);
					rem = 0;
					if (prod >= 10 && i != 0) {
						while (prod >= 10) {
							prod -= 10;
							rem += 1;
						}
					} result = Integer.toString(prod) + result;
				} return new apint(result);
			}
		} else {
			for (int i = 0; i < size; i++) {
                        	int num = Character.getNumericValue(string2.charAt(i));
                        	result = (this.multiply(new apint(num))).toString();
                        	for (int j = size - i - 1; j > 0; j--) {
                                	result += "0";
                        	}
				product = product.add(new apint(result));
                	} return product;
		}
	}

	// division function
        public apint divide(apint value) {
		String str_dividend = this.toString();
                String str_divisor = value.toString();
                String quotient = "";
		if (this.first() == '-' && value.first() != '-') {
                        apint temp = new apint((str_dividend).substring(1));
                        quotient = (temp.divide(value)).toString();
                        return new apint("-" + quotient);
                } if (this.first() != '-' && value.first() == '-') {
                        apint temp = new apint((str_divisor).substring(1));
                        quotient = (this.divide(temp)).toString();
                        return new apint("-" + quotient);
                } if (this.first() == '-' && value.first() == '-') {
                        apint temp1 = new apint((str_dividend).substring(1));
                        apint temp2 = new apint((str_divisor).substring(1));
                        return temp1.divide(temp2);
		} int quotientSize = str_dividend.length() - str_divisor.length();
		if (quotientSize < 0 || str_divisor == "0") {
			return new apint();
		} else if (quotientSize == 0) {
			int endBound;
			if (str_dividend.length() >= 19) {
				endBound = 19;
			} else {
				endBound = str_dividend.length();
			} String temp1 = str_dividend.substring(0, endBound);
			if (str_divisor.length() >= 19) {
                                endBound = 19;
                        } else {
                                endBound = str_divisor.length();
                        } String temp2 = str_divisor.substring(0, endBound); 
                        if (Long.parseLong(temp1) < Long.parseLong(temp2)) {
				return new apint();
			} else if (Long.parseLong(temp1) >= Long.parseLong(temp2)) {
				return new apint(1);
			}
		} int index = str_divisor.length() + 1;
                apint dividend = new apint(str_dividend.substring(0, index));
                apint divisor = new apint(str_divisor);
                for (int i = 0; i < quotientSize; i++) {
			int counter = 0;
                        while ((dividend.subtract(divisor.multiply(new apint(counter)))).first() != '-') {
				counter++;
			} counter -= 1;
			quotient += (Integer.toString(counter));
			dividend = dividend.subtract(divisor.multiply(new apint(counter)));
			String temp = dividend.toString();
			if (index <= (str_dividend.length() - 1)) {
				temp += str_dividend.charAt(index);
				dividend = new apint(temp);
				index++;
			}
                } return new apint(quotient);
        }

	public static void main(String args[]) {
		// Constructor Tests
		System.out.println("Constructor Tests:");
		apint test1 = new apint();
		apint test2 = new apint("-11112222333344445555666677778888999900000000999988887777666655554444333322221111");
		apint test3 = new apint(1000000000);
		apint test4 = new apint(1000000000.6574839201);
		apint test5 = new apint("+12345678901234567890");
		apint test6 = new apint("+55555666666");
		apint test7 = new apint("-12345678901234567890");
		apint test8 = new apint("-55555666666");
		apint test9 = new apint("+999999999999999999");
		apint test10 = new apint("-999999999999999999");
		test1.print();
		test2.print();
		test3.print();
		test4.print();
		test5.print();
		test6.print();
		test7.print();
		test8.print();
		test9.print();
		test10.print();

		// Addition Tests
		System.out.println("\nAddition Tests:");
		apint addTest1 = test5.add(test6);
		apint addTest2 = test6.add(test5);
		apint addTest3 = test7.add(test6);
		apint addTest4 = test6.add(test7);
		apint addTest5 = test5.add(test8);
		apint addTest6 = test8.add(test5);
		apint addTest7 = test7.add(test8);
		apint addTest8 = test8.add(test7);
		apint addTest9 = test9.add(test9);
		apint addTest10 = test9.add(test10);
		apint addTest11 = test10.add(test9);
		apint addTest12 = test10.add(test10);
		addTest1.print();
		addTest2.print();
		addTest3.print();
		addTest4.print();
		addTest5.print();
		addTest6.print();
		addTest7.print();
		addTest8.print();
		addTest9.print();
		addTest10.print();
		addTest11.print();
		addTest12.print();

		// Subtraction Tests
		System.out.println("\nSubtraction Tests:");
		apint subTest1 = test5.subtract(test6);
		apint subTest2 = test6.subtract(test5);
		apint subTest3 = test7.subtract(test6);
		apint subTest4 = test6.subtract(test7);
		apint subTest5 = test5.subtract(test8);
		apint subTest6 = test8.subtract(test5);
		apint subTest7 = test7.subtract(test8);
		apint subTest8 = test8.subtract(test7);
		apint subTest9 = test9.subtract(test9);
		apint subTest10 = test9.subtract(test10);
		apint subTest11 = test10.subtract(test9);
		apint subTest12 = test10.subtract(test10);
		subTest1.print();
		subTest2.print();
		subTest3.print();
		subTest4.print();
		subTest5.print();
		subTest6.print();
		subTest7.print();
		subTest8.print();
		subTest9.print();
		subTest10.print();
		subTest11.print();
		subTest12.print();

		//  Multiplication Tests
		System.out.println("\nMultiplication Tests:");
		apint multTest1 = test5.multiply(test6);
		apint multTest2 = test6.multiply(test5);
		apint multTest3 = test7.multiply(test6);
		apint multTest4 = test6.multiply(test7);
		apint multTest5 = test5.multiply(test8);
		apint multTest6 = test8.multiply(test5);
		apint multTest7 = test7.multiply(test8);
		apint multTest8 = test8.multiply(test7);
		apint multTest9 = test9.multiply(test9);
		apint multTest10 = test9.multiply(test10);
		apint multTest11 = test10.multiply(test9);
		apint multTest12 = test10.multiply(test10);
		multTest1.print();
		multTest2.print();
		multTest3.print();
		multTest4.print();
		multTest5.print();
		multTest6.print();
		multTest7.print();
		multTest8.print();
		multTest9.print();
		multTest10.print();
		multTest11.print();
		multTest12.print();

		// Division Tests
		System.out.println("\nDivision Tests:");
		apint divTest1 = test5.divide(test6);
		apint divTest2 = test6.divide(test5);
		apint divTest3 = test7.divide(test6);
		apint divTest4 = test6.divide(test7);
		apint divTest5 = test5.divide(test8);
		apint divTest6 = test8.divide(test5);
		apint divTest7 = test7.divide(test8);
		apint divTest8 = test8.divide(test7);
		apint divTest9 = test9.divide(test9);
		apint divTest10 = test9.divide(test10);
		apint divTest11 = test10.divide(test9);
		apint divTest12 = test10.divide(test10);
		divTest1.print();
		divTest2.print();
		divTest3.print();
		divTest4.print();
		divTest5.print();
		divTest6.print();
		divTest7.print();
		divTest8.print();
		divTest9.print();
		divTest10.print();
		divTest11.print();
		divTest12.print();

		// Extra Credit: 1000!
		System.out.println("\nExtra Credit: 1000!");
		apint factorial = new apint(1);
		for (int i = 1; i <= 1000; i++) {
			factorial = factorial.multiply(new apint(i));
		} String hugeFactorial = factorial.toString();
		int index = (int) Math.ceil(hugeFactorial.length() / 60);
		File file = new File("/home/kenny/CMPS101/CMPS101S18PA1/ExtraCredit.txt");
		PrintWriter out = null;
		try {
		out = new PrintWriter(file);
		for (int i = 0; i < index; i++) {
				if (i != index) {
					out.println(hugeFactorial.substring(60 * i, 60 * (i + 1)));
				} else {
					out.println(hugeFactorial.substring(60 * i));
				}
			}
		} catch (java.io.FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (out != null) {
				out.close();
				System.out.println("The value of 1000! output to ExtraCredit.txt!");
			}
		}
	}
}
