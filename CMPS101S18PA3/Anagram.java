import java.util.*;

public class Anagram {
	
	// Anagram data members
	private static HashMap<String, ArrayList<String>> map;
	private String m_word;
	private apint m_value;
	
	// Parameterized constructor with string parameter
	public Anagram(String p_word) {
		m_word = p_word;
		m_value = primeProduct(p_word);
	}

	// Parameterized constructor with char array
	public Anagram(char[] p_word) {
		String newWord = String.valueOf(p_word);
		Anagram temp = new Anagram(newWord);
		map = temp.map;
		setWord(temp.getWord());
		m_value = temp.m_value;
	}

	// Print method
	public void print() {
		String key = (Anagram.primeProduct(m_word)).toString();
		if (map.containsKey(key) && map.get(key).size() > 1) {
			System.out.println("Anagrams for " + m_word + ":");
			ArrayList<String> list = map.get(key);
			for (int i = 0; i < list.size(); i++) {
				if (!(list.get(i).equals(m_word))) {
					System.out.println(list.get(i));
				}
			}
		} else {
			System.out.println("There are no anagrams for " + m_word);
		}
	}

	// Compare method
	public int compare(Anagram other) {
		char first = m_value.subtract(other.m_value).first();
		if (first != '-' && first != '0') {
			return 1;
		} else if (first == '0') {
			return 0;
		} else {
			return -1;
		}
	}

	// Getter function for string member
        public String getWord() {
                return m_word;
        }

        // Setter function for string member
        public void setWord(String p_word) {
                m_word = p_word;
		m_value = primeProduct(p_word);
        }

	// Setter function for map member
	public void setMap(HashMap<String, ArrayList<String>> newMap) {
		map = new HashMap<String, ArrayList<String>>(newMap);
	}

	// Static method to find the nth prime number
        // This is not the fastest way to do this. However, this program only needs to
        // find up to the 26th prime number, so this method works just fine for that.
        public static int prime_n(int n) {
                int num, counter, i;
                num = counter = 1;
                while (counter < n + 1) {
                        num++;
                        for (i = 2; i <= num; i++) {
                                if (num % i == 0) { break; }
                        } if (i == num) {
                                counter++;
                        }
                } return num;
        }

        // Static method to find the unique product of primes for a string.
        // This is used to provide key values for our wordList.txt HashMap.
        public static apint primeProduct(String word) {
                apint product = new apint(1);
                for (int i = 0; i < word.length(); i++) {
                        apint prime = new apint(prime_n(word.charAt(i) - 96));
                        product = product.multiply(prime);
                } return product;
        }
}
