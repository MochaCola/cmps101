Script started on 2019-02-23 15:18:32-0800
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ pwd
/home/kenny/CMPS101/CMPS101S18PA3
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ ls -l
total 1912
-rw-r--r-- 1 kenny kenny    2871 Feb 23 15:04 Anagram.java
-rw-r--r-- 1 kenny kenny   12221 Feb 22 14:31 apint.java
-rw-r--r-- 1 kenny kenny    5128 Feb 23 15:06 FindAnagrams.java
-rw-r--r-- 1 kenny kenny    1495 Feb 22 14:31 NoteToGrader
-rw-r--r-- 1 kenny kenny       0 Feb 23 15:18 pa3submissionfile.txt
-rw-r--r-- 1 kenny kenny     590 Feb 22 14:31 README
-rw-rw-r-- 1 kenny kenny 1923525 Feb 20 20:40 wordList.txt
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ cat README
Directory path: /home/kenny/CMPS101/CMPS101S18PA3

Directory files:

	Source files:
		Anagram.java -		The file containing the methods for the Anagram class.

	Test files:
		FindAnagrams.java -	The file containing the test code.

	Other files:

		README - 		This file, documenting all of the files in this directory.
		NoteToGrader -		A short note to the grader describing my approach.
		apint.java -            The file containing the methods for the apint class (used for implementation of Anagram).
                wordList.txt -          The text file containing the long list of words.
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ cat NoteToGrader 
         -         -        -        -         -         -         -         -         -         -
Hurray! An assignment submitted on time, with plenty of time to spare, no less!

For this program, I decided to implement my Anagram class with a HashMap, which has key objects of
Strings, and value objects of ArrayList<String>. Here is my reasoning for both parameters:

- String keys:	Using the method prime_n in Anagram.java, I assigned a unique prime value to each
 		character of the alphabet. So, each string / char array assigned to an Anagram
		object would also be assigned a unique product of primes value. Any anagrams of
		a word will share this value. So, by using apints, I can find this potentially
		massive value, and convert it to a string using the overridden toString() method
		in apint.java. This numeric string is what I use to group anagrams in the
		HashMap that I create for each Anagram object.

- ArrayList<String>:	Using an ArrayList of Strings as the value objects in the HashMap allows
			me to group up Strings with like product of prime values. This will
			create an ArrayList of anagrams where applicable, which makes my job
			much easier! (The motto: work harder so you can be lazy!) 

I hope you're pleased with my test cases. I thought that it would be satisfactory to create
two Anagram objects - one with a String, the other with a char Array - and then compare them.
This process can be repeated as many times as the user likes.

Best wishes,
Kenny Blum
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ cat A[KF[KAnagram.java 
import java.util.*;

public class Anagram {
	
	// Anagram data members
	private static HashMap<String, ArrayList<String>> map;
	private String m_word;
	private apint m_value;
	
	// Parameterized constructor with string parameter
	public Anagram(String p_word) {
		m_word = p_word;
		m_value = primeProduct(p_word);
	}

	// Parameterized constructor with char array
	public Anagram(char[] p_word) {
		String newWord = String.valueOf(p_word);
		Anagram temp = new Anagram(newWord);
		map = temp.map;
		setWord(temp.getWord());
		m_value = temp.m_value;
	}

	// Print method
	public void print() {
		String key = (Anagram.primeProduct(m_word)).toString();
		if (map.containsKey(key) && map.get(key).size() > 1) {
			System.out.println("Anagrams for " + m_word + ":");
			ArrayList<String> list = map.get(key);
			for (int i = 0; i < list.size(); i++) {
				if (!(list.get(i).equals(m_word))) {
					System.out.println(list.get(i));
				}
			}
		} else {
			System.out.println("There are no anagrams for " + m_word);
		}
	}

	// Compare method
	public int compare(Anagram other) {
		char first = m_value.subtract(other.m_value).first();
		if (first != '-' && first != '0') {
			return 1;
		} else if (first == '0') {
			return 0;
		} else {
			return -1;
		}
	}

	// Getter function for string member
        public String getWord() {
                return m_word;
        }

        // Setter function for string member
        public void setWord(String p_word) {
                m_word = p_word;
		m_value = primeProduct(p_word);
        }

	// Setter function for map member
	public void setMap(HashMap<String, ArrayList<String>> newMap) {
		map = new HashMap<String, ArrayList<String>>(newMap);
	}

	// Static method to find the nth prime number
        // This is not the fastest way to do this. However, this program only needs to
        // find up to the 26th prime number, so this method works just fine for that.
        public static int prime_n(int n) {
                int num, counter, i;
                num = counter = 1;
                while (counter < n + 1) {
                        num++;
                        for (i = 2; i <= num; i++) {
                                if (num % i == 0) { break; }
                        } if (i == num) {
                                counter++;
                        }
                } return num;
        }

        // Static method to find the unique product of primes for a string.
        // This is used to provide key values for our wordList.txt HashMap.
        public static apint primeProduct(String word) {
                apint product = new apint(1);
                for (int i = 0; i < word.length(); i++) {
                        apint prime = new apint(prime_n(word.charAt(i) - 96));
                        product = product.multiply(prime);
                } return product;
        }
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ javac FindAnagrams.java 
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ ls 0[K-l
total 1936
-rw-r--r-- 1 kenny kenny    2804 Feb 23 15:19 Anagram.class
-rw-r--r-- 1 kenny kenny    2871 Feb 23 15:04 Anagram.java
-rw-r--r-- 1 kenny kenny    6380 Feb 23 15:19 apint.class
-rw-r--r-- 1 kenny kenny   12221 Feb 22 14:31 apint.java
-rw-r--r-- 1 kenny kenny    4303 Feb 23 15:19 FindAnagrams.class
-rw-r--r-- 1 kenny kenny    5128 Feb 23 15:06 FindAnagrams.java
-rw-r--r-- 1 kenny kenny    1495 Feb 22 14:31 NoteToGrader
-rw-r--r-- 1 kenny kenny    4096 Feb 23 15:18 pa3submissionfile.txt
-rw-r--r-- 1 kenny kenny     590 Feb 22 14:31 README
-rw-rw-r-- 1 kenny kenny 1923525 Feb 20 20:40 wordList.txt
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ cat FindAnagrams.
cat: FindAnagrams.: No such file or directory
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ ca [Kt FindAnagrams.java 
import java.util.*;
import java.io.*;

public class FindAnagrams {
	
	// Static method to initialize HashMap
        // This will read each individual word from the specified path and find its
        // prime product, which will be used as the HashMap's key. The HashMap's
        // value will be an ArrayList of words with similar prime products
        public static HashMap<String, ArrayList<String>> createHashMap(String path) {
                HashMap<String, ArrayList<String>> map;
		map = new HashMap<String, ArrayList<String>>();
		BufferedReader reader = null;
                try {
                        System.out.println("-------------- CONSTRUCTING HASHMAP -------------");
                        FileReader file = new FileReader(path);
                        reader = new BufferedReader(file);
                        String word = reader.readLine();        // first readLine will be 173528
                        System.out.println(word + " words found in " + path + ".");
                        ArrayList<String> list;
                        System.out.println("Processing... this may take some time.");
                        while ((word = reader.readLine()) != null) {
                                list = new ArrayList<String>();
                                apint key = Anagram.primeProduct(word);
                                if (map.containsKey(key.toString())) {
                                        list = map.get(key.toString());
                                }
                                list.add(word);
                                map.put(key.toString(), list);
                        }
                        System.out.println("The HashMap contains " + map.size() + " elements.");
                        System.out.println("------------- HASHMAP CONSTRUCTED! --------------\n");
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                reader.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                } return map;
        }
	
	public static void main(String[] args) {
		
		HashMap<String, ArrayList<String>> map;
		map = createHashMap(args[0]);			// HashMap for Anagram variables
		Scanner input = new Scanner(System.in);		// Scanner for user input
		String value = "";				// String for constructor test
		String again = "";				// String for "do again?" entry
		char[] charArray;				// Char array for constructor test
		Anagram var1 = null;				// Anagram for string constructor test
		Anagram var2 = null;				// Anagram for char array constructor test
		
		while (!again.equals("n")) {	// Will loop if user does not specify "n" or "N"
			
			
			System.out.println("------------ STRING CONSTRUCTOR TEST ------------");
			System.out.println("Enter a string: ");
			value = (input.nextLine()).toLowerCase();	// Converts to lowercase string
			if (var1 == null) {			// New HashMap if var1 has never been initialized
				var1 = new Anagram(value);
				var1.setMap(map);
			} else {
				var1.setWord(value);		// Otherwise, use the same HashMap
			}
			var1.print();	// Prints all anagram(s)
			System.out.println("-------- STRING CONSTRUCTOR SUCCESSFUL! ---------\n");
			
			
			System.out.println("\n---------- CHAR ARRAY CONSTRUCTOR TEST ----------");
			System.out.println("Enter an array of characters: ");
                        charArray = ((input.nextLine()).toLowerCase()).toCharArray();
                        if (var2 == null) {
                                var2 = new Anagram(charArray);	// New HashMap if var2 has never been initialized
                                var2.setMap(map);
                        } else {
                                var2.setWord(String.valueOf(charArray));	// Otherwise, use the same HashMap
                        }
                        var2.print();	// Prints all anagram(s)
			System.out.println("------ CHAR ARRAY CONSTRUCTOR SUCCESSFUL! -------\n");


			System.out.println("\n------------- COMPARISON METHOD TEST ------------");
			System.out.println("Comparing string Anagram to char array Anagram...");
			int comparison = var1.compare(var2);
			if (comparison == 1) {
				System.out.println("Anagram 1 has a greater value than Anagram 2");
			} else if (comparison == 0) {
				System.out.println("Anagram 1 and Anagram 2 are anagrams!");
			} else {
				System.out.println("Anagram 2 has a greater value than Anagram 1");
			} System.out.println("--------- COMPARISON METHOD SUCCESSFUL! ---------\n");

			
			System.out.println("Do another round of tests? (y/n)");
			again = (input.nextLine()).toLowerCase();	// Converts to lowercase string
			System.out.print("\n");
			while (!again.equals("y") && !again.equals("n")) {	// Will repeat if invalid entry
				System.out.println("Invalid input.\nDo another round of tests? (y/n)");
				again = (input.nextLine()).toLowerCase();
				System.out.print("\n");
			}
		} System.out.println("Thank you, come again!");		// Program end
	}
}
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ java FindAnagrams wordList.txtt 
-------------- CONSTRUCTING HASHMAP -------------
173528 words found in wordList.txt.
Processing... this may take some time.
The HashMap contains 157077 elements.
------------- HASHMAP CONSTRUCTED! --------------

------------ STRING CONSTRUCTOR TEST ------------
Enter a string: 
spear
Anagrams for spear:
apers
apres
asper
pares
parse
pears
prase
presa
rapes
reaps
spare
-------- STRING CONSTRUCTOR SUCCESSFUL! ---------


---------- CHAR ARRAY CONSTRUCTOR TEST ----------
Enter an array of characters: 
o items
Anagrams for items:
emits
metis
mites
smite
stime
times
------ CHAR ARRAY CONSTRUCTOR SUCCESSFUL! -------


------------- COMPARISON METHOD TEST ------------
Comparing string Anagram to char array Anagram...
Anagram 2 has a greater value than Anagram 1
--------- COMPARISON METHOD SUCCESSFUL! ---------

Do another round of tests? (y/n)
Y

------------ STRING CONSTRUCTOR TEST ------------
Enter a string: 
dangered
Anagrams for dangered:
deranged
gandered
gardened
-------- STRING CONSTRUCTOR SUCCESSFUL! ---------


---------- CHAR ARRAY CONSTRUCTOR TEST ----------
Enter an array of characters: 
angered
Anagrams for angered:
derange
enraged
grandee
grenade
------ CHAR ARRAY CONSTRUCTOR SUCCESSFUL! -------


------------- COMPARISON METHOD TEST ------------
Comparing string Anagram to char array Anagram...
Anagram 1 has a greater value than Anagram 2
--------- COMPARISON METHOD SUCCESSFUL! ---------

Do another round of tests? (y/n)
y

------------ STRING CONSTRUCTOR TEST ------------
Enter a string: 
posts
Anagrams for posts:
spots
stops
-------- STRING CONSTRUCTOR SUCCESSFUL! ---------


---------- CHAR ARRAY CONSTRUCTOR TEST ----------
Enter an array of characters: 
spots
Anagrams for spots:
posts
stops
------ CHAR ARRAY CONSTRUCTOR SUCCESSFUL! -------


------------- COMPARISON METHOD TEST ------------
Comparing string Anagram to char array Anagram...
Anagram 1 and Anagram 2 are anagrams!
--------- COMPARISON METHOD SUCCESSFUL! ---------

Do another round of tests? (y/n)
No!

Invalid input.
Do another round of tests? (y/n)
N

Thank you, come again!
]0;kenny@kenny-VirtualBox: ~/CMPS101/CMPS101S18PA3[01;32mkenny@kenny-VirtualBox[00m:[01;34m~/CMPS101/CMPS101S18PA3[00m$ exit
exit

Script done on 2019-02-23 15:20:40-0800
