import java.util.*;
import java.io.*;

public class FindAnagrams {
	
	// Static method to initialize HashMap
        // This will read each individual word from the specified path and find its
        // prime product, which will be used as the HashMap's key. The HashMap's
        // value will be an ArrayList of words with similar prime products
        public static HashMap<String, ArrayList<String>> createHashMap(String path) {
                HashMap<String, ArrayList<String>> map;
		map = new HashMap<String, ArrayList<String>>();
		BufferedReader reader = null;
                try {
                        System.out.println("-------------- CONSTRUCTING HASHMAP -------------");
                        FileReader file = new FileReader(path);
                        reader = new BufferedReader(file);
                        String word = reader.readLine();        // first readLine will be 173528
                        System.out.println(word + " words found in " + path + ".");
                        ArrayList<String> list;
                        System.out.println("Processing... this may take some time.");
                        while ((word = reader.readLine()) != null) {
                                list = new ArrayList<String>();
                                apint key = Anagram.primeProduct(word);
                                if (map.containsKey(key.toString())) {
                                        list = map.get(key.toString());
                                }
                                list.add(word);
                                map.put(key.toString(), list);
                        }
                        System.out.println("The HashMap contains " + map.size() + " elements.");
                        System.out.println("------------- HASHMAP CONSTRUCTED! --------------\n");
                } catch (IOException e) {
                        e.printStackTrace();
                } finally {
                        try {
                                reader.close();
                        } catch (IOException e) {
                                e.printStackTrace();
                        }
                } return map;
        }
	
	public static void main(String[] args) {
		
		HashMap<String, ArrayList<String>> map;
		map = createHashMap(args[0]);			// HashMap for Anagram variables
		Scanner input = new Scanner(System.in);		// Scanner for user input
		String value = "";				// String for constructor test
		String again = "";				// String for "do again?" entry
		char[] charArray;				// Char array for constructor test
		Anagram var1 = null;				// Anagram for string constructor test
		Anagram var2 = null;				// Anagram for char array constructor test
		
		while (!again.equals("n")) {	// Will loop if user does not specify "n" or "N"
			
			
			System.out.println("------------ STRING CONSTRUCTOR TEST ------------");
			System.out.println("Enter a string: ");
			value = (input.nextLine()).toLowerCase();	// Converts to lowercase string
			if (var1 == null) {			// New HashMap if var1 has never been initialized
				var1 = new Anagram(value);
				var1.setMap(map);
			} else {
				var1.setWord(value);		// Otherwise, use the same HashMap
			}
			var1.print();	// Prints all anagram(s)
			System.out.println("-------- STRING CONSTRUCTOR SUCCESSFUL! ---------\n");
			
			
			System.out.println("\n---------- CHAR ARRAY CONSTRUCTOR TEST ----------");
			System.out.println("Enter an array of characters: ");
                        charArray = ((input.nextLine()).toLowerCase()).toCharArray();
                        if (var2 == null) {
                                var2 = new Anagram(charArray);	// New HashMap if var2 has never been initialized
                                var2.setMap(map);
                        } else {
                                var2.setWord(String.valueOf(charArray));	// Otherwise, use the same HashMap
                        }
                        var2.print();	// Prints all anagram(s)
			System.out.println("------ CHAR ARRAY CONSTRUCTOR SUCCESSFUL! -------\n");


			System.out.println("\n------------- COMPARISON METHOD TEST ------------");
			System.out.println("Comparing string Anagram to char array Anagram...");
			int comparison = var1.compare(var2);
			if (comparison == 1) {
				System.out.println("Anagram 1 has a greater value than Anagram 2");
			} else if (comparison == 0) {
				System.out.println("Anagram 1 and Anagram 2 are anagrams!");
			} else {
				System.out.println("Anagram 2 has a greater value than Anagram 1");
			} System.out.println("--------- COMPARISON METHOD SUCCESSFUL! ---------\n");

			
			System.out.println("Do another round of tests? (y/n)");
			again = (input.nextLine()).toLowerCase();	// Converts to lowercase string
			System.out.print("\n");
			while (!again.equals("y") && !again.equals("n")) {	// Will repeat if invalid entry
				System.out.println("Invalid input.\nDo another round of tests? (y/n)");
				again = (input.nextLine()).toLowerCase();
				System.out.print("\n");
			}
		} System.out.println("Thank you, come again!");		// Program end
	}
}
